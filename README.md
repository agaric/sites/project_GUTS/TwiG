# TeachersWithGUTS.org

## Clone the repository and bring up the virtual development environment

```
$ git clone git@gitlab.com:project_GUTS/TwiG.git
$ cd TwiG
$ ddev start
$ ddev auth ssh
```

See general recommended Git setup: https://docs.agaric.coop/tools/git-setup.html

### Pull in others' changes


```
ddev composer install
ddev . rake sync_from_live (replace "live" with test if you want that)
ddev . rake css
ddev . drush -y cim
```

If there is content on the live site that you need for testing - To pull in other developers' configuration changes without destroying
content, after `composer install` you can use:

```
drush -y cim
```

### Export configuration

```
$ drush -y cex
```

And then `git add config` and review diff (`git diff --cached`) before committing.


### Adding contributed modules

With `composer require ...` you can add new dependencies to your installation:

```
$ ddev composer require drupal/honeypot
```

### Deployment

[Jenkins' continuous integration](https://ci.agaric.com/job/GUTS/) automatically deploys to the [test site](http://stage.teacherswithguts.org/) if tests pass.

**Before** deploying to production, check for any live changes to configuration.  You can use `drush @live uli` to log in, and then visit https://teacherswithguts.org/admin/config/development/configuration

Any configuration changes that matter will need to be exported and the additions and updates committed to the `config` directory.

```
ddev . rake deploy ENVIRONMENT=test
```

And of course the same thing with `live` to deploy the production site.

If you're doing this for the first time, you'll need to `mkdir build` first.
