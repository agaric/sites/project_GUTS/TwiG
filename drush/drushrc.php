<?php
$command_specific['site-install'] = array(
  'site-name' => 'Teachers with GUTS',
  'account-pass' => 'admin',
);

$command_specific['dcer'] = array(
  'folder' => 'profiles/guts/content',
);

$options['structure-tables']['common'] = array('cache_*', 'history', 'search_*', 'sessions');
