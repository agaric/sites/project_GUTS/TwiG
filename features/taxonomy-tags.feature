Feature: Tags taxonomy

  @api
  Scenario: A content editor can edit tags taxonomy
    Given I am logged in as a user with the "content_editor" role
    When I go to "/admin/structure/taxonomy/manage/tags/overview" 
    Then the response status code should be 200
    And I should see "add term" 

  @api
  Scenario: A content editor can add a tags taxonomy term
    Given I am logged in as a user with the "content_editor" role
    When I go to "/admin/structure/taxonomy/manage/tags/add" 
    And I fill in "Name" with "Those short things" 
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Created new term Those short things." 





























