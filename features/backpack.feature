Feature: Save a bookmark

  @api
  Scenario: A logged-in user can save a page in his backpack
    Given I am logged in as a user with the "authenticated" role
    When I go to "/resources/love-train"
    Then the response status code should be 200
    And I should see "Add to backpack"
    And I click "Add to backpack"
    Then the response status code should be 200
    And I should see "Add bookmark"
    And I press the "Save" button
    Then I go to "/my-bookmarks"
    And I should see "Love train"
    Then I follow "Delete" in the row containing "Love train"
    And I should see "You don't have any bookmarks yet. Add one!"

