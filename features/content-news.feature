Feature: Create news

  @api
  Scenario: An Administrator user can publish news.
    Given I am logged in as a user with the "administrator" role
    When I go to "node/add/news"
    Then the response status code should be 200
    And I should see "Create News"
    And I fill in "Title" with "News title"
    And I fill in "Body" with "Read about this, it is very useful."
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "News News title has been created"
    And I should see "Discuss this news"

    # A forum post should be created for this news item
    When I click "Discuss this news"
    Then I should see "Discussions"
    And I should see "News title"
    # The forum topic should be created in the correct forum.
    And I should see "News discussion"

    # Check that we are still offered to discuss when discussion exists.
    When I click "View the news"
    Then the response status code should be 200
    And I should see "Discuss this news"
