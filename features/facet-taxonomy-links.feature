Feature: Facet links.

  @api
  Scenario: Click a link to filter by an specific Content type.
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/resources"
    And I follow "Contributed Curriculum Modules"
    Then the url should match "/resources/search"
    And I should see "(-) Contributed Curriculum Modules"


  @api
  Scenario: Click a link to filter by an content area
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/resources"
    And I follow "Computer Science"
    Then the url should match "/resources/search"
    And I should see "(-) Computer Science"

  @api
  Scenario: Click a link to filter by an specific Tool.
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/resources"
    And I follow "StarLogo TNG"
    Then the url should match "/resources/search"
    And I should see "(-) StarLogo TNG"

  @api
  Scenario: Click a link to filter by an specific Tool.
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/resources"
    And I follow "StarLogo TNG"
    Then the url should match "/resources/search"
    And I should see "(-) StarLogo TNG"

  @api
  Scenario: Click a link to filter by an specific Use type.
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/resources"
    And I follow "Middle School"
    Then the url should match "/resources/search"
    And I should see "(-) Middle School"


   @api
  Scenario: Click any link no related with a facet.
    Given I am logged in as a user with the "authenticated user" role
    When I go to "/taxonomy/term/40"
    Then the response status code should be 200
    And I should see the heading "School"


