Feature: Forums feed view

  @visitor @feed
  Scenario: A visitor can reach the forum feed page
    Given I am on the homepage
    When I follow "Discussions"
    And I follow "Feed view"
    Then I should see the heading "Feed View"
    And I should see the heading "Best Subject for teaching"

  @api @feed
  Scenario: An authenticated user can post a comment to a forum post.
    Given I am logged in as a user with the "authenticated" role
    When I follow "Discussions"
    And I follow "Feed view"
    Then I follow "Best Subject for teaching"
    And I should see the heading "Add new comment"

    When I fill in "Subject" with "This is a test comment"
    And I fill in "Comment" with "What's wrong with a comment?"
    And I press "Save"
    Then I should see the heading "This is a test comment"
    And I should see the text "What's wrong with a comment?"

  @api @feed
  Scenario: An authenticated user can post a forum topic from the Feed view
    Given I am logged in as a user with the "authenticated" role
    When I follow "Discussions"
    And I follow "Feed view"
    Then I should see the heading "What's on your mind?"
    When I fill in "Subject" with "Arbitration clauses at for-profit schools"
    And I select "StarLogo TNG" from "Forum"
    And I press "Save"
    Then the response status code should be 200
    # And I should see the success message "Arbitration clauses at for-profit schools"
    And I should see the heading "Arbitration clauses at for-profit schools"
