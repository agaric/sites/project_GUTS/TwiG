Feature: The user shoudn't be redirected after to save his profile.

  @api
  Scenario: A User save his profile.
    Given I am logged in as a user with the authenticated role
    When I go to "/user"
    And I click "Update Profile"
    And I select "US" from "field_address[0][address][country_code]"
    And I press the "Save" button
    # The address fields are loaded using Ajax and we haven't JS in our tests
    # so first we will fail the form so all the fields can be rendered,
    # and then fill the missing fields.
    Then I fill in "field_address[0][address][locality]" with "Santa Fe"
    And I fill in "field_address[0][address][postal_code]" with "90401"
    And I fill in "field_address[0][address][administrative_area]" with "CA"
    And I fill in "field_organization[0][value]" with "Agaric"
    And I fill in "field_first_name[0][value]" with "First Name"
    And I fill in "field_last_name[0][value]" with "Last Name"
    And I fill in "field_short_bio[0][value]" with "Biography"
    And I press the "Save" button
    And I should see the message 'The changes have been saved.'
    # The first time to the user save his profile it must be redirected to the
    # homepage.
    And the url should match "/"
    # The second time the user will continue in the same page.
    Then I go to "/user"
    And I click "Update Profile"
    And I press the "Save" button
    And the url should match "/user/[0-9]+/edit"
    And I should see the message 'The changes have been saved.'

    # Test the "Receive comment follow-up notifications emails" checkbox is working.
    Then I go to "/user"
    And I click "Update Profile"
    And I check the box "Receive comment follow-up notification emails"
    And I press the "Save" button
    And I should see the message 'The changes have been saved.'
    And the "Receive comment follow-up notification emails" checkbox should be checked

    Then I go to "/user"
    And I click "Update Profile"
    And I uncheck the box "Receive comment follow-up notification emails"
    And I press the "Save" button
    And I should see the message 'The changes have been saved.'
    And the "Receive comment follow-up notification emails" checkbox should not be checked
    

