Feature: Roadmap

  @visitor
  Scenario: A visitor can reach the New to GUTS roadmap from the resources page
    When I am on "/resources"
    And I follow "new to GUTS" in the div headlined "New to GUTS"
    Then I should see the heading "New to GUTS?"

  @visitor
  Scenario: A visitor can reach the Familiar with GUTS? roadmap from the resources page
    When I am on "/resources"
    And I follow "familiar with GUTS" in the div headlined "Familiar with GUTS?"
    Then I should see the heading "Familiar with GUTS?"

  @visitor
  Scenario: A visitor can reach the Experienced with GUTS? roadmap from the resources page
    When I am on "/resources"
    And I follow "experienced with GUTS" in the div headlined "Experienced with GUTS?"
    Then I should see the heading "Experienced with GUTS?"

  # We need JavaScript to see and reach the contextual Edit link.
  @api
  @javascript
  Scenario: A content editor can use the roadmap contextual links
    Given I am logged in as a user with the "content_editor" role
    And I go to "/resources"
    When I press the "Open Familiar with GUTS? configuration options" button
    And I follow "Edit" in the div headlined "Familiar with GUTS?"
    Then I should be on "/block/2"

  # JavaScript makes it annoying to fill in the rich text area.
  @api
  Scenario: A content editor can edit the featured resources block
    Given I am logged in as a user with the "content_editor" role
    And I am on "/block/2"
    Then the response status code should be 200
    And I should see the heading "Edit custom block Familiar with GUTS?" 
    When I fill in "Body" with "<p>All-new familiar guts body text.&nbsp; But we need to keep the link to not break other tests on subsequent runs: Are you <a href='/node/7'>familiar with GUTS</a>?</p>"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see the success message "Basic block Familiar with GUTS? has been updated."

