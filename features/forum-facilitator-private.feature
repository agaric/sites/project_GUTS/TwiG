Feature: Forums

  @visitor
  Scenario: A visitor cannot see the private facilitator forum listed
    Given I am on the homepage
    When I follow "Discussions"
    Then I should see the heading "Forums"
    #    And I should not see the link "Private facilitator forum"

  @api
  Scenario: A logged-in user cannot see the private facilitator forum listed
    Given I am logged in as a user with the "authenticated" role
    And I am on the homepage
    When I follow "Discussions"
    Then I should see the heading "Forums"
    #    And I should not see the link "Private facilitator forum"

  @api
  Scenario: A facilitator can see the private facilitator forum listed
    Given I am logged in as a user with the "facilitator" role
    And I am on the homepage
    When I follow "Discussions"
    Then I should see the heading "Forums"
    And I should see the link "Private facilitator forum"
    When I follow "Private facilitator forum"
