Feature: Send Digest-Email

  @api
  Scenario: An user receive and email digest.
    Given I am logged in as a user with the "authenticated" role
    When I go to "/node/add/forum?forum_id=0"
    Then the response status code should be 200
    And I fill in "Subject" with "Test Forum Topic"
    And I fill in "Body" with "It is the body."
    And I select "General discussion" from "Forum"
    And I fill in "Tags" with "School"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Forum topic Test Forum Topic has been created"

    # Visit the digest view to see if it contains the Forum topic
    When I go to the digest debug view with date "yesterday"
    Then I should see "Test Forum Topic"
