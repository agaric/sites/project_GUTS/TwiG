Feature: Forums

  @visitor
  Scenario: A visitor can reach the forum page
    Given I am on the homepage
    When I follow "Discussions"
    Then I should see the heading "Forums"

  @api
  Scenario: A logged-in user can see the forum page
    Given I am logged in as a user with the "authenticated" role
    And I am on the homepage
    When I follow "Discussions"
    Then I should see the heading "Forums"

  @api
  Scenario: A logged-in user can post topics on the forum page and comments
    Given I am logged in as a user with the "authenticated" role
    And I am on the homepage
    When I follow "Discussions"
    And  I follow "Forum view"
    Then I should see the link "Add New Forum Topic"
    When I follow "Add New Forum Topic"
    Then I am on "/node/add/forum"
    And  the response status code should be 200
    When I fill in "Subject" with "mytitle"
    And I fill in "Body" with "mybody"
    And I select "General discussion" from "Forum"
    And I press "Save"
    Then the response status code should be 200
    And I should see the heading "mytitle"
    And I should see the message "Forum topic mytitle has been created."
    Then I am logged in as a user with the "authenticated" role
    And I am on "/discussions/mytitle"
    Then I should see the text "Add new comment"
    When I fill in "Comment" with "mycomment"
    And I press "Save"
    Then I should see the message "Your comment has been posted."
    Then I am on "/user/logout"
    And I am on "/discussions/mytitle"
    And I should see the heading "Comments"
    And I should see the text "mycomment"
