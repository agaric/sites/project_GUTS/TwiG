Feature: The user should be able to register.

  @api
  Scenario: A User wants to create a new account.
    Given I go to "/user/register"
    And I select "US" from "field_address[0][address][country_code]"
    And I press the "Create new account" button
    # The address fields are loaded using Ajax and we haven't JS in our tests
    # so first we will fail the form so all the fields can be rendered,
    # and then fill the missing fields.
    Then I fill in "mail" with "behat-test@agaric.com"
    And I fill in "name" with "behat test"
    And I fill in "field_first_name[0][value]" with "Walter"
    And I fill in "field_last_name[0][value]" with "White"
    And I fill in "field_organization[0][value]" with "Agaric"
    And I fill in "field_short_bio[0][value]" with "Biography"
    And I fill in "field_address[0][address][locality]" with "Santa Fe"
    And I fill in "field_address[0][address][postal_code]" with "90401"
    And I fill in "field_address[0][address][administrative_area]" with "CA"
    And I fill in "field_organization[0][value]" with "Agaric"
    And I fill in "field_first_name[0][value]" with "First Name"
    And I fill in "field_last_name[0][value]" with "Last Name"
    And I fill in "field_short_bio[0][value]" with "Biography"
    And I press the "Create new account" button
    And I should see the message 'Thank you for applying for an account.'
    Given I am logged in as a user with the "administrator" role
    And I go to "/admin/people"
    And I follow "Edit" in the row containing "behat test"
    And the "displays[personal_digest_news:block_1][enabled]" checkbox should be checked
    And the "displays[resources:block_1][enabled]" checkbox should be checked
    And the "displays[activity:block_1][enabled]" checkbox should be checked
    