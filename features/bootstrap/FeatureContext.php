<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /** @BeforeSuite */
  public static function BeforeSuite($event) {
    // Set MailChimp in test mode.
    \Drupal::configFactory()->getEditable('mailchimp.settings')->set('test_mode', TRUE)->save();
  }

  /**
   * @When /^I follow "([^"]*)" in the row containing "([^"]*)"$/
   */
  public function iFollowLinkInTheRowContaining($linkName, $rowText) {
      /** @var $row \Behat\Mink\Element\NodeElement */
      $row = $this->getSession()->getPage()->find('css', sprintf('table tr:contains("%s")', $rowText));
      if (!$row) {
          throw new \Exception(sprintf('Cannot find any row on the page containing the text "%s"', $rowText));
      }
      $row->clickLink($linkName);
  }

  /**
   * @When /^I follow "([^"]*)" in the div headlined "([^"]*)"$/
   */
  public function iFollowLinkInDivHeadlined($linkName, $divHeader) {
      /** @var $div \Behat\Mink\Element\NodeElement */
      $h2 = $this->getSession()->getPage()->find('css', sprintf('div h2:contains("%s")', $divHeader));
      $div = $h2->getParent();
      if (!$div) {
          throw new \Exception(sprintf('Cannot find any div on the page with H2 "%s"', $divHeader));
      }
      $div->clickLink($linkName);
  }

  /**
   * @When I go to the digest debug view with date :arg1
   */
  public function iGoToTheDigestDebugViewWithDate($arg1)
  {
    $date = date('Y-m-d', strtotime($arg1));
    $this->getSession()->visit($this->locatePath('/digest-debug/' . $date));
  }

  /**
   * @Then should filter by :arg1
   */
  public function shouldFilterBy($arg1)
  {
    $current_url = $this->getSession()->getCurrentUrl();
    if (strpos($current_url, $arg1) !== FALSE) {
      return;
    }

    throw new \Behat\Mink\Exception\ExpectationException("The filter wasn't found in the URL", $this->getSession());
  }


  /** @AfterFeature */
  public static function teardownFeature($event)
  {
    $email = "behat-test@agaric.com";

    // Delete any test user.
    if ($user = user_load_by_mail($email)) {
      user_delete($user->id());
    }

  }

  /** @AfterSuite */
  public static function AfterSuite($event) {
    // Set MailChimp in production mode.
    \Drupal::configFactory()->getEditable('mailchimp.settings')->set('test_mode', FALSE)->save();
  }

  /**
   * @Then I should see the image alt text :arg1
   */
  public function assertAlt($alt) {
    $images = $this->getSession()->getPage()->findAll('css', 'img');
    if (empty($images)) {
      throw new \Exception(sprintf('No images found on the page %s', $this->getSession()->getCurrentUrl()));
    }
    foreach ($images as $img) {
      if (strpos($img->getAttribute('alt'), $alt) !== FALSE) {
        return $img;
      }
    }
    throw new \Exception(sprintf('Failed to find an image with alt text containing "%s" on the page %s', $alt, $this->getSession()->getCurrentUrl()));
  }

}
