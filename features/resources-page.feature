Feature: Check that the resources page is working.

  @api
  Scenario: A user can click in the resources links.
    Given I am at "/resources"
    Then I click "High School"
    And should filter by "education_level"
    And I should see "An example link resource"
    When I go to "/resources"
    Then I click "Lesson Plan"
    And should filter by "content_type"
    And I should see "An example resource"
    When I go to "/resources"
    Then I click "Computer Science"
    And should filter by "curricular_area"
    And I should see "An example resource"
    When I go to "/resources"
    Then I click "StarLogo TNG"
    And should filter by "tools"
    And I should see "An example resource"
    And I should see "An example link resource"

