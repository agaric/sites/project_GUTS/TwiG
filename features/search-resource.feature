Feature: Search results for resources

  @api
  Scenario: A visitor can search resources
    Given I am at "/resources/search"
    Then I should see "Find Resources"
    When I fill in "keyword" with "An example link resource"
    # TODO update this for new unified search
    #    And I press the "edit-submit-search-resources-content" button
    #    Then I should see the link "An example link resource"

  @api
  Scenario: A visitor can filter using the Find resource block
    Given I am at "/resources/search"
    Then I click "High School (1)"
    And should filter by "education_level"
    And I should see "An example link resource"
    When I go to "/resources/search"
    Then I click "Lesson Plan (1)"
    And should filter by "content_type"
    And I should see "An example resource"
    When I go to "/resources/search"
    Then I click "Computer Science (1)"
    And should filter by "curricular_area"
    And I should see "An example resource"
    When I go to "/resources/search"
    Then I click "StarLogo TNG (2)"
    And should filter by "tools"
    And I should see "An example resource"
    And I should see "An example link resource"
