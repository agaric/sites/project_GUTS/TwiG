Feature: Check the user cannot save more than 500 characters in the summary.

  @api
  Scenario: A User visit the add resource page.
    Given I am logged in as a user with the authenticated role
    When I go to "/node/add/resource"
    Then the response status code should be 200
    And I should see "Create Resource"
    And I fill in "Title" with "My Excellent Resource"
    And I fill in "field_summary[0][value]" with "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nunc elit, porta vel massa sit amet, eleifend molestie tellus. Proin quis sollicitudin felis. Suspendisse dignissim fermentum sem, vel commodo purus malesuada vel. Donec elementum varius accumsan. Vivamus sollicitudin sit amet massa nec commodo. Sed ornare sit amet nulla id iaculis. Suspendisse potenti. Vivamus sit amet vulputate erat.Sed placerat turpis sit amet neque lacinia, id viverra risus dictum. Nam volutpat erat orci, eu consectetur diam congue sed. Sed pellentesque mollis nisi. Quisque turpis velit, rutrum vel metus metus. sdw"
    And I fill in "Body" with "This is not the teaser, it is the body."
    And I fill in "Original author" with "Ned Flanders"
    And I select "Handout" from "Content type"
    And I fill in "Tags" with "School"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Error message The Summary cannot have more than 500 characters"
    And I should not see "Resource My Excellent Resource has been created"
