Feature: Resource with and without discussion

  Scenario: A visitor is offered to log in to start a discussion on resources without a discussion yet
    Given I am an anonymous user
    When I go to "/resources/this-node-dont-carry-no-gamblers"
    Then the response status code should be 200
    And I should see the heading "This node don't carry no gamblers"
    And I should see "Log in or register to view attachments and related links, and/or join the discussion"

  @api
  Scenario: A visitor is offered to see a discussion on a resource with one.
    Given I am an anonymous user
    When I go to "/resources/love-train"
    Then the response status code should be 200
    And I should see the heading "Love train"
    And I should see "Log in or register to view attachments and related links, and/or join the discussion"
