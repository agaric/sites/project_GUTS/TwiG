Feature: Menu link feature

  @api
  Scenario: An authenticated user can see my pages link
    Given I am logged in as a user with the "authenticated" role
    When I am on the homepage 
    And I should see the link "My pages" in the "header" region 

  @api
    Scenario: An anonymous user cannot see my pages link
    Given I am not logged in
    When I am on the homepage                                   
    And I should not see the link "My pages" in the "header" region 




























