Feature: Edit featured resources

  # We need JavaScript to see and reach the contextual Edit link.
  @api
  @javascript
  Scenario: A content editor can use the featured resources contextual link
    Given I am logged in as a user with the "content_editor" role
    And I go to "/resources"
    When I press the "Open Featured resources configuration options" button
    And I follow "Edit" in the div headlined "Featured resources"
    Then I should be on "/block/5"

  # JavaScript makes it annoying to fill in the rich text area.
  @api
  Scenario: A content editor can edit the featured resources block
    Given I am logged in as a user with the "content_editor" role
    And I am on "/block/5"
    Then the response status code should be 200
    And I should see the heading "Edit custom block Featured resources"
    When I fill in "Body" with "All-new featured resources body text"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see the success message "Basic block Featured resources has been updated."

