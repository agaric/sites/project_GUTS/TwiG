Feature: The user should be able to see another user profile.

  @api
  Scenario: A User wants to view other user profile.
    Given users:
      | name     | mail              | status |
      | user1    | user1@example.com | 1      |
    And I am logged in as a user with the "authenticated" role
    When I visit "members/user1"
    Then the response status code should be 200
    And I should see the text "This member has not yet shared any content."

