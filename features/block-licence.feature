Feature: Block Licence

  Scenario: Any visitor can see the licence from any page
    Given I am on the homepage
    Then I should see the text "Unless otherwise licensed, all works accessible here are licensed under a Creative Commons Attribution-ShareAlike 4.0 International License." in the "content" region
    And  I should see the text "Teachers with GUTS is partially funded by the National Science Foundation." in the "content" region

