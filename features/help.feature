Feature: Help page

  @visitor
  Scenario: A visitor can reach the help page
    Given I am on the homepage
    When I follow "Help" in the "header" region
    Then I should see the text "How to: Join the OPDN community"

  @api
  Scenario: A logged-in user can reach the help page
    Given I am logged in as a user with the "content_editor" role
    And I am on the homepage
    When I follow "Help" in the "header" region
    Then I should see the text "How to: Join the OPDN community"
