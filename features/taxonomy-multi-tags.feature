Feature: Tags multi-taxonomy

  @api
  Scenario: An authenticated user can add or edit a resource
    Given I am logged in as a user with the "authenticated user" role
    When I go to "node/add/resource" 
    Then the response status code should be 200
    And I should see "Create Resource" 

  @api
  Scenario: An authenticated user can add multiple tags taxonomy term
    Given I am logged in as a user with the "authenticated user" role
    When I go to "node/add/resource" 
    And I fill in "Title" with "My Excellent Resource"
    And I fill in "Summary" with "My lovely and wonderful resource is here. Read about it."
    And I fill in "Body" with "This is not the teaser, it is the body."
    And I fill in "Original author" with "Ned Flanders"
    And I select "Handout" from "Content type"
    And I fill in "Tags" with "School, map, polling"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "School map polling"





























