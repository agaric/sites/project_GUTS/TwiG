Feature: Resource-related taxonomy

  @api
  Scenario: A content editor can edit the Use type taxonomy
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Use type"
    Then the response status code should be 200
    And I should see "Middle School"
    And I should see "High School"
    And I should see "Professional Development"
    And I should see "Other"

  @api
  Scenario: A content editor can add an Use type taxonomy term
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Use type"
    And I follow "Add term"
    Then the response status code should be 200
    And I fill in "Name" with "Those short things"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Created new term Those short things."

  @api
  Scenario: A content editor can edit Content Type taxonomy
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Content Type"
    Then the response status code should be 200
    And I should see "Activity"
    And I should see "Advocacy Material"
    And I should see "Assessment"
    And I should see "Contributed Curriculum Modules"
    And I should see "GUTS Curriculum Modules"
    And I should see "Handout"
    And I should see "Lesson Plan"
    And I should see "Online Course"
    And I should see "Presentation"
    And I should see "Reference Guide"
    And I should see "Research"
    And I should see "Sample Project"
    And I should see "Tutorial"
    And I should see "Other"
    And I should see "Video"
    And I should not see "Albatross the ultimate taxonomy term"

  @api
  Scenario: A content editor can add an Content Type taxonomy term
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Content Type"
    And I follow "Add term"
    Then the response status code should be 200
    And I fill in "Name" with "Those short things"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Created new term Those short things."

  @api
  Scenario: A content editor can edit Curricular Area taxonomy
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Curricular Area"
    Then the response status code should be 200
    And I should see "Computer Science"
    And I should see "Engineering"
    And I should see "Mathematics"
    And I should see "Science- Life Sciences"
    And I should see "Science- Earth Sciences"
    And I should see "Science- Physical Sciences"
    And I should see "Science- Social Sciences"
    And I should see "Other"

  @api
  Scenario: A content editor can add a Curricular Area taxonomy term
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Curricular Area"
    And I follow "Add term"
    Then the response status code should be 200
    And I fill in "Name" with "Science- Astrology"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Created new term Science- Astrology."

    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Curricular Area"
    Then the response status code should be 200
    And I should see "Science- Astrology"

  @api
  Scenario: A content editor can edit Tools taxonomy
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Tools"
    Then the response status code should be 200
    And I should see "StarLogo TNG"
    And I should see "StarLogo Nova"
    And I should see "NetLogo"
    And I should see "Other"

  @api
  Scenario: A content editor can add a Tools taxonomy term
    Given I am logged in as a user with the "content_editor" role
    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Tools"
    And I follow "Add term"
    Then the response status code should be 200
    And I fill in "Name" with "Hammers"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Created new term Hammers."

    When I follow "Structure" in the "toolbar" region
    And I follow "Taxonomy"
    And I follow "List terms" in the row containing "Tools"
    Then the response status code should be 200
    And I should see "Hammers"
