Feature: Create resource

  @api
  Scenario: An authenticated user can upload resources
    Given I am logged in as a user with the "authenticated" role
    When I go to "/node/add/resource"
    Then the response status code should be 200
    And I should see "Create Resource"
    And I fill in "Title" with "My Excellent Resource"
    And I fill in "Summary" with "My lovely and wonderful resource is here. Read about it."
    And I fill in "Body" with "This is not the teaser, it is the body."
    And I fill in "Original author" with "Ned Flanders"
    And I select "Handout" from "Content type"
    And I fill in "Tags" with "School"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Resource My Excellent Resource has been created"
    And I should see "Discuss this resource"

    # A forum post should be created for this resource
    When I click "Discuss this resource"
    Then I should see "Discussions"
    And I should see "My Excellent Resource"
    # The forum topic should be created in the correct forum.
    #### TODO we are not showing the current forum because
    #### A) the link is wrong (gets redirected to resoures) and
    #### B) it is styled poorly, with the tag-style badge
    #### And I should see "Contributed Resources"

    # Check the text has changed correctly in the resource.
    When I click "View the resource"
    Then the response status code should be 200
    And I should see "Discuss this resource"



  @api
  Scenario: An Overlord can edit posted date; dates are formatted.
    Given I am logged in as a user with the "administrator" role
    When I go to "/node/add/resource"
    Then the response status code should be 200
    And I should see "Create Resource"
    When I fill in "Title" with "My Back-dated Resource"
    And I fill in "Summary" with "This resource is backdated."
    And I fill in "Body" with "This is the body of the backdated resource."
    And I select "Handout" from "Content type"
    And I fill in "Date" with "2016-10-26"
    And I press the "Save" button
    Then the response status code should be 200
    And I should see "Resource My Back-dated Resource has been created"
    And I should see "October 26, 2016"
