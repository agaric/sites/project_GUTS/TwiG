#!/usr/bin/env drush
<?php

/**
 * This script should be run using Drush.
 * drush scr reset-email-digest.php
 */

$param = drush_shift();
$uid = (is_null($param)) ? 1 : $param;
\Drupal::state()->delete('personal_digest_last_cron');

// Delete the last value so the email can be send again.
$data = Drupal::service('user.data')->get('personal_digest', $uid, 'digest');
unset($data['last']);
Drupal::service('user.data')->set('personal_digest', $uid, 'digest', $data);
