#!/usr/bin/env drush
<?php

/**
 * This script should be run using Drush.
 * drush scr reset-email-digest.php
 */

$param = drush_shift();
$date = drush_shift();
$uid = (is_null($param)) ? 1 : $param;
\Drupal::state()->delete('personal_digest_last_cron');

// Update the date of the last time the digest was sent.
$data = Drupal::service('user.data')->get('personal_digest', $uid, 'digest');

// Number of dats since the last cron ran.
if (is_null($date) || !is_numeric($date)) {
    $date = strtotime('-1 day');
}

$data['last'] = strtotime("-{$date} days");
Drupal::service('user.data')->set('personal_digest', $uid, 'digest', $data);
