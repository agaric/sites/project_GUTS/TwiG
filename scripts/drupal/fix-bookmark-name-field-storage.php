#!/usr/bin/env drush
<?php

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Re-install the name field definition to allow 255 characteres.
 */
function bookmark_fix_name_field_definition() {
  $database = \Drupal::database();

  // Retrieve existing entity data.
  $query = $database->select('bookmark_field_data', 'bfd');
  $query->fields('bfd', ['id', 'name']);
  $bookmarks_data = $query->execute()->fetchAllKeyed();

  // Remove data from the storage.
  $database->update('bookmark_field_data')
    ->fields(['name' => NULL])
    ->execute();

  $manager = \Drupal::entityDefinitionUpdateManager();
  $original_definition = $manager->getFieldStorageDefinition('name', 'bookmark');
  $new_definition = BaseFieldDefinition::create('string')
    ->setLabel(t('Name'))
    ->setDescription(t('The name of the Bookmark entity.'))
    ->setSettings([
      'max_length' => 250,
      'text_processing' => 0,
    ]);

  // Delete the old one.
  $manager->uninstallFieldStorageDefinition($original_definition);
  // Install the new one.
  $manager->installFieldStorageDefinition('name', 'bookmark', 'bookmark', $new_definition);

  // Restore name data in the new schema.
  foreach ($bookmarks_data as $id => $name) {
    $database->update('bookmark_field_data')
      ->fields(['name' => $name])
      ->condition('id', $id)
      ->execute();
  }

}

bookmark_fix_name_field_definition();
