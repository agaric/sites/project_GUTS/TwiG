require "rake/clean"

if not ENV.key?("ENVIRONMENT") then
  ENV["ENVIRONMENT"] = "dev"
end

if not ['dev', 'live', 'test'].include?(ENV['ENVIRONMENT']) then
  print "Not a valid environment! Please specify dev, live, or test.\n"
  exit
end

BUILD_DIR = "build"
DEFAULT_DIR = "web/sites/default"
PROFILE = "guts"
SETTINGS_SOURCE_REL = "provisioning/settings.#{ENV['ENVIRONMENT']}.php"
SETTINGS_TARGET_REL = "#{DEFAULT_DIR}/settings.local.php"
# Note: use ENVIRONMENT test|live, not stage|production
TEST = "root@stage.teacherswithguts.org"
PROD = "root@www.teacherswithguts.org"
DEPLOY_TARGET_TEST = "/var/www/stage"
DEPLOY_TARGET_PROD = "/var/www/drupal"

if ENV["ENVIRONMENT"] == "dev" then
  SETTINGS_SOURCE = SETTINGS_SOURCE_REL
  SETTINGS_TARGET = SETTINGS_TARGET_REL
else
  SETTINGS_SOURCE = "#{BUILD_DIR}/#{SETTINGS_SOURCE_REL}"
  SETTINGS_TARGET = "#{BUILD_DIR}/#{SETTINGS_TARGET_REL}"
end

if ENV["ENVIRONMENT"] == "test" then
  DEPLOY_TARGET = "#{TEST}:#{DEPLOY_TARGET_TEST}"
elsif ENV["ENVIRONMENT"] == "live" then
  DEPLOY_TARGET = "#{PROD}:#{DEPLOY_TARGET_PROD}"
end

CLEAN.include(["#{BUILD_DIR}/*"])

file SETTINGS_TARGET => SETTINGS_SOURCE do
  cp SETTINGS_SOURCE, SETTINGS_TARGET
end

task :test => ["test:behat"]

task :vendor do
  sh "composer install"
end

desc "Compile CSS."
task :css do
  sh "sass -E 'utf-8' --line-numbers web/themes/kidneys/sass/kidneys.scss web/themes/kidneys/css/kidneys.css"
end

task :default => ["test"]

desc "Install Drupal."
task :drupal => [:vendor, :css, SETTINGS_TARGET] do
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web -y si #{PROFILE} install_configure_form.update_status_module='[0, 0]'"
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web -y cim"
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web cr"
  sh "./vendor/bin/drush cache-clear drush"
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web search-api-reindex resources_content_index"
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web cron"
  sh "./vendor/bin/drush -r #{Dir.pwd.shellescape}/web cr"
  chmod_R "u+w", DEFAULT_DIR
end

task :build  do
  rm_r BUILD_DIR
  mkdir_p BUILD_DIR
  sh "git archive HEAD | tar -x -C #{BUILD_DIR}"
  sh "composer install -d #{BUILD_DIR} --no-dev"
  sh "git describe --always --long HEAD > #{BUILD_DIR}/web/BUILD_VERSION.txt"
  cp SETTINGS_SOURCE, SETTINGS_TARGET
  sh "sass -E 'utf-8' #{BUILD_DIR}/web/themes/kidneys/sass/kidneys.scss #{BUILD_DIR}/web/themes/kidneys/css/kidneys.css"
end

task :deploy_config => [:build] do
  sh "rsync -rz --delete #{BUILD_DIR}/config/ #{DEPLOY_TARGET}/config/"
end

task :deploy_web => [:build] do
  sh "rsync -rz --delete --exclude sites/default/files #{BUILD_DIR}/web/ #{DEPLOY_TARGET}/web/"
end

task :deploy_vendor => [:build] do
  sh "rsync -rz --delete --links #{BUILD_DIR}/vendor/ #{DEPLOY_TARGET}/vendor/"
end

task :deploy_composer => [:build] do
  sh "scp #{BUILD_DIR}/composer.json #{DEPLOY_TARGET}/composer.json"
end

desc "Deploy code base - specify ENVIRONMENT={test|live}"
task :deploy => [:deploy_config, :deploy_vendor, :deploy_web, :deploy_composer] do
  sh "#{BUILD_DIR}/vendor/bin/drush @#{ENV['ENVIRONMENT']} --alias-path=#{BUILD_DIR}/drush/ -y updb"
  sh "#{BUILD_DIR}/vendor/bin/drush @#{ENV['ENVIRONMENT']} --alias-path=#{BUILD_DIR}/drush/ -y cim"
  sh "#{BUILD_DIR}/vendor/bin/drush @#{ENV['ENVIRONMENT']} --alias-path=#{BUILD_DIR}/drush/ cr"
end

desc "Copy database and files from live site to test."
task :sync_live_to_test do
  sh "echo '** Note, temporary SQL files are saved local to rake in /tmp. If you are in a container and need these for some reason, get them before the container is stopped.'"
  # This is not working - may be fixable by ensuring drush aliases work from the context of the live sites? drush -y sql-sync @live @test, so:
  sh "./vendor/bin/drush -y @test sql-dump > /tmp/paranoia.sql"
  sh "./vendor/bin/drush -y @live sql-dump > /tmp/twg-copy.sql"
  sh "./vendor/bin/drush -y @test sql-drop"
  sh "./vendor/bin/drush -y @test sqlc < /tmp/twg-copy.sql"
  sh "ssh -A #{PROD} rsync -rz --stats --exclude styles \
      --exclude css --exclude js #{DEPLOY_TARGET_PROD}/web/sites/default/files/ \
      --delete #{DEPLOY_TARGET_TEST}/web/sites/default/files/"
end

namespace :test do
  desc "Run Behat tests."
  task :behat => [:vendor] do
    sh "./vendor/bin/behat --tags '~@javascript&&~@visitor&&~@feed' --format pretty --out std --format junit --out reports"
  end

  desc "Run performance tests."
  task :performance => [:vendor] do
    sh "./vendor/bin/phpunit --bootstrap ./vendor/autoload.php --log-junit reports/performance.xml tests/"
  end
end
