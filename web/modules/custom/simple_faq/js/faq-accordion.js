(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.faqAccordion = {
    attach: function (context, settings) {
      // Using once() to apply the myCustomBehaviour effect when you want to do just run one function.
      $(once('myCustomBehavior', $(context).find('.faq-accordion-wrapper'))).each ( function() {
        $(this).accordion({
          header: "h2.faq-accordion-header",
          heightStyle: "content"
        });
      });
    }
  };

})(jQuery, Drupal);
