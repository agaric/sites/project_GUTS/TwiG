<?php

namespace Drupal\guts_reports;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * UsersReport Class.
 */
class GutsBaseReport extends ControllerBase {

  /**
   * The related request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Guts Reports Builder.
   *
   * @var \Drupal\guts_reports\GutsReportsBuilder
   */
  protected $gutsReportsBuilder;

  /**
   * UsersReport constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The related request stack.
   */
  public function __construct(RequestStack $requestStack, GutsReportsBuilder $gutsReportsBuilder) {
    $this->requestStack = $requestStack;
    $this->gutsReportsBuilder = $gutsReportsBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('guts_reports_builder')
    );
  }

  /**
   * This get the header tables array and return the column names.
   *
   * @param array $headers
   *   The table headers.
   */
  protected function cleanTableHeaders($headers) {
    // The headers have an special format to build the table, let's clean them.
    $result = [];
    foreach ($headers as $header) {
      $result[] = (string)$header['data'];
    }
    return $result;
  }

  /**
   * Generate and download the CSV.
   *
   * @param $filename
   *   The name of the file that is going to be downloaded.
   * @param $rows
   *   Array of arrays, each row represent one line of the CSV file.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  protected function downloadCSVFile($filename, $rows) {
    $temp = file_directory_temp() . '/' . $filename;
    $f = fopen($temp, 'w');
    foreach ($rows as $line) {
      fputcsv($f, $line, ',');
    }
    fclose($f);
    return new BinaryFileResponse($temp, 200, [], TRUE, 'attachment');
  }

}
