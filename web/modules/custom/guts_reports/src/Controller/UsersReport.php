<?php

namespace Drupal\guts_reports\Controller;

use Drupal\Core\Link;
use Drupal\guts_reports\Form\UsersReportForm;
use Drupal\guts_reports\GutsBaseReport;

/**
 * UsersReport Class.
 */
class UsersReport extends GutsBaseReport {
  /**
   * Returns the Users Report Table.
   *
   * @return array
   *   Render Array.
   */
  public function content() {
    $request = $this->requestStack->getCurrentRequest();
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');
    $role = $request->get('role');
    $cohort_group = $request->get('cohort_group');
    $start_date = (!empty($start_date)) ? strtotime($start_date . ' 00:00:00') :  strtotime(UsersReportForm::DEFAULT_START_DATE);
    $end_date = (!empty($end_date)) ? strtotime($end_date . ' 23:59:59') : strtotime(UsersReportForm::DEFAULT_END_DATE);
    $role = (empty($role)) ? UsersReportForm::DEFAULT_ROLE : $role;
    $cohort_group = (empty($cohort_group)) ? UsersReportForm::COHORT_GROUP : $cohort_group;
    $format = $request->get('format');

    $users = $this->gutsReportsBuilder->getUsersReport([
      'start_date' => $start_date,
      'end_date' => $end_date,
      'role' => $role,
      'cohort_group' => $cohort_group,
    ]);

    $total = $this->gutsReportsBuilder->getUsesTotalRow([
      'start_date' => $start_date,
      'end_date' => $end_date,
      'role' => $role,
      'cohort_group' => $cohort_group,
    ]);

    $build['controls'] = $this->formBuilder()->getForm(UsersReportForm::class);
    $headers = [
      ['data' => $this->t('Name'), 'field' => 'name'],
      ['data' => $this->t('# Logins'), 'field' => 'logins', 'sort' => 'desc', 'descending' => TRUE],
      ['data' => $this->t('# Pages Viewed'), 'field' => 'views', 'descending' => TRUE],
      ['data' => $this->t('# Click Throughs'), 'field' => 'clicks', 'descending' => TRUE],
      ['data' => $this->t('# Downloads'), 'field' => 'downloads', 'descending' => TRUE],
      ['data' => $this->t('# Uploads'), 'field' => 'uploads', 'descending' => TRUE],
      ['data' => $this->t('# Comments'), 'field' => 'responses', 'descending' => TRUE],
      ['data' => $this->t('# Backpack stores'), 'field' => 'backpack_stores', 'descending' => TRUE],
      ['data' => $this->t('# Blog posts'), 'field' => 'blog_posts', 'descending' => TRUE],
      ['data' => $this->t('# Resources'), 'field' => 'resources', 'descending' => TRUE],
      ['data' => $this->t('# Forum topics'), 'field' => 'forum_topic', 'descending' => TRUE],
    ];

    if (!empty($format) && $format == 'csv') {
      return $this->generateCSVFile(
        $users,
        $total,
        $headers,
        'users_' . date('Y-m-d' , $start_date) . '_' . date('Y-m-d', $end_date) . '_' . $role . '_' . $cohort_group . '.csv'
      );
    }

    $build['help_text'] = [
      '#type' => 'fieldset',
      '#title' => t('Help:'),
    ];
    $build['help_text']['list'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        [
          '#type' => 'markup',
          '#markup' => '<strong># of comments:</strong> are comments left on any content type',
        ],
        [
          '#type' => 'markup',
          '#markup' => '<strong># forum topics:</strong> are the number of new forum topic pages created in the Discussions section of the site.',
        ],
      ],
    ];

    $build['users'] = [
      '#type' => 'table',
      '#header' => $headers,
    ];
    $users->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($headers);
    $users = $users->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $users->limit(15);
    $users = $users->execute()->fetchAll();
    $totals = $total->execute()->fetch();
    array_unshift($users, $totals);
    foreach ($users as $key => $user) {
      if (isset($user->name)) {
        $build['users'][$key]['name'] = Link::createFromRoute($user->name, 'entity.user.canonical', ['user' => $user->uid])->toRenderable();
      }
      else {
        $build['users'][$key]['name'] = [
          '#plain_text' => $this->t('Total'),
        ];
      }
      $build['users'][$key]['logins'] = [
        '#plain_text' => $user->logins,
      ];
      $build['users'][$key]['views'] = [
        '#plain_text' => $user->views,
      ];
      $build['users'][$key]['clicks'] = [
        '#plain_text' => $user->clicks,
      ];
      $build['users'][$key]['downloads'] = [
        '#plain_text' => $user->downloads,
      ];
      $build['users'][$key]['uploads'] = [
        '#plain_text' => $user->uploads,
      ];
      $build['users'][$key]['responses'] = [
        '#plain_text' => $user->responses,
      ];
      $build['users'][$key]['backpack_stores'] = [
        '#plain_text' => $user->backpack_stores,
      ];
      $build['users'][$key]['blog_posts'] = [
        '#plain_text' => $user->blog_posts,
      ];
      $build['users'][$key]['resources'] = [
        '#plain_text' => $user->resources,
      ];
      $build['users'][$key]['discussions_started'] = [
        '#plain_text' => $user->forum_topic,
      ];

      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

  /**
   * Create and download a CSV file version of the report.
   *
   * @param $users
   * @param $total
   * @param array $headers
   */
  protected function generateCSVFile($users, $total, $headers, $filename) {
    $users = $users->execute()->fetchAll();
    $totals = $total->execute()->fetch();
    array_unshift($users, $totals);
    // Add the headers as the first line.
    $rows[] = $this->cleanTableHeaders($headers);
    foreach ($users as $key => $user) {
      $rows[] = [
        isset($user->name) ? $user->name : 'TOTAL',
        $user->logins,
        $user->views,
        $user->clicks,
        $user->downloads,
        $user->uploads,
        $user->responses,
        $user->backpack_stores,
        $user->blog_posts,
        $user->resources,
        $user->forum_topic,
      ];
    }

    return $this->downloadCSVFile($filename, $rows);
  }
}
