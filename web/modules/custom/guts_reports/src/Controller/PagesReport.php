<?php

namespace Drupal\guts_reports\Controller;

use Drupal\Core\Link;
use Drupal\guts_reports\Form\PagesReportForm;
use Drupal\guts_reports\Form\UsersReportForm;
use Drupal\guts_reports\GutsBaseReport;

/**
 * PagesReport Class.
 */
class PagesReport extends GutsBaseReport {

  /**
   * Returns the Pages Report Table.
   *
   * @return array
   *   Render Array.
   */
  public function content() {
    $request = $this->requestStack->getCurrentRequest();
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');
    $content_type = $request->get('content_type');
    $format = $request->get('format');

    $start_date = (!empty($start_date)) ? strtotime($start_date . ' 00:00:00') :  strtotime(UsersReportForm::DEFAULT_START_DATE);
    $end_date = (!empty($end_date)) ? strtotime($end_date . ' 23:59:59') : strtotime(UsersReportForm::DEFAULT_END_DATE);
    // @todo create it's own constant with the default value.
    $content_type = (empty($content_type)) ? 'all' : $content_type;

    $build['controls'] = $this->formBuilder()->getForm(PagesReportForm::class);

    $pages = $this->gutsReportsBuilder->getPagesReport([
      'start_date' => $start_date,
      'end_date' => $end_date,
      'content_type' => $content_type,
    ]);

    $total = $this->gutsReportsBuilder->getPagesTotalRow([
      'start_date' => $start_date,
      'end_date' => $end_date,
      'content_type' => $content_type,
    ]);

    $headers = [
      ['data' => $this->t('Page name'), 'field' => 'title'],
      ['data' => $this->t('# Views'), 'field' => 'views', 'sort' => 'desc', 'descending' => TRUE],
      ['data' => $this->t('# Backpack stores'), 'field' => 'backpack_stores', 'descending' => TRUE],
      ['data' => $this->t('# Click Throughs'), 'field' => 'clicks', 'descending' => TRUE],
      ['data' => $this->t('# Comments'), 'field' => 'responses', 'descending' => TRUE],
      ['data' => $this->t('# Uploads'), 'field' => 'uploads', 'descending' => TRUE],
      ['data' => $this->t('# Downloads'), 'field' => 'downloads', 'descending' => TRUE],
    ];

    if (!empty($format) && $format == 'csv') {
      return $this->generateCSVFile(
        $pages,
        $total,
        $headers,
        'pages_' . date('Y-m-d' , $start_date) . '_' . date('Y-m-d', $end_date) . '_' . $content_type . '.csv'
      );
    }

    $build['pages'] = [
      '#type' => 'table',
      '#header' => $headers,
    ];

    $build['pages'] = [
      '#type' => 'table',
      '#header' => $headers,
    ];
    $pages->extend('Drupal\Core\Database\Query\TableSortExtender')->orderByHeader($headers);
    $pages = $pages->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $pages->limit(15);
    $pages = $pages->execute()->fetchAll();
    $totals = $total->execute()->fetch();
    array_unshift($pages, $totals);
    foreach ($pages as $key => $page) {
      if (isset($page->title)) {
        $build['pages'][$key]['title'] = Link::createFromRoute($page->title, 'entity.node.canonical', ['node' => $page->nid])
          ->toRenderable();
      }
      else {
        $build['pages'][$key]['title'] = [
          '#plain_text' => $this->t('Total'),
        ];
      }
      $build['pages'][$key]['views'] = [
        '#plain_text' => $page->views,
      ];
      $build['pages'][$key]['backpack_stores'] = [
        '#plain_text' => $page->backpack_stores,
      ];
      $build['pages'][$key]['clicks'] = [
        '#plain_text' => $page->clicks,
      ];
      $build['pages'][$key]['responses'] = [
        '#plain_text' => $page->responses,
      ];
      $build['pages'][$key]['uploads'] = [
        '#plain_text' => $page->uploads,
      ];
      $build['pages'][$key]['downloads'] = [
        '#plain_text' => $page->downloads,
      ];
    }
      $build['pager'] = [
        '#type' => 'pager',
      ];

    return $build;
  }


  /**
   * Create and download a CSV file version of the report.
   *
   * @param $pages
   * @param $total
   * @param array $headers
   */
  protected function generateCSVFile($pages, $total, $headers, $filename) {
    $pages = $pages->execute()->fetchAll();
    $totals = $total->execute()->fetch();

    array_unshift($pages, $totals);
    // Add the headers as the first line.
    $rows[] = $this->cleanTableHeaders($headers);
    foreach ($pages as $key => $page) {
      $rows[] = [
        isset($page->title) ? $page->title : 'TOTAL',
        $page->views,
        $page->backpack_stores,
        $page->clicks,
        $page->responses,
        $page->uploads,
        $page->downloads,
      ];
    }

    return $this->downloadCSVFile($filename, $rows);

  }
}
