<?php

/**
 * GutsReports Service.
 */
namespace Drupal\guts_reports;

use Drupal\Core\Database\Connection;

class GutsReportsBuilder {

  /**
   * The database connection to use.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * GutsReports constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Connection object to get the Teachers data.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Returns a single object with the total of the report.
   *
   * @param $options
   *   The total row will be filtered using these options.
   */
  public function getUsesTotalRow($options) {
    $start_date = $options['start_date'];
    $end_date = $options['end_date'];
    $role = $options['role'];
    $cohort_group = $options['cohort_group'];

    $login_select = $this->getLoginSelect($start_date, $end_date);
    $downloads_select = $this->getDownloadsSelect($start_date, $end_date);
    $uploads_select = $this->getUploadsSelect($start_date, $end_date);
    $blog_posts_select = $this->getBlogPostSelect($start_date, $end_date);
    $bookmarks_select = $this->getBookmarksSelect($start_date, $end_date);
    $resources_select = $this->getResourcesSelect($start_date, $end_date);
    $forum_topic_select = $this->getForumTopicSelect($start_date, $end_date);
    $clicks_select = $this->getClicksSelect($start_date, $end_date);
    $views_select = $this->getViewsSelect($start_date, $end_date);
    $responses_select = $this->getDiscussionsResponseSelect($start_date, $end_date);
    $user_role_select = $this->getUserRoleSelect();
    $user_cohort_select = $this->getCohortSelect();

    $query = $this->connection->select('users_field_data', 'ufd');
    $query->addExpression('SUM(COALESCE(lh.logins, 0))', 'logins');
    $query->addExpression('SUM(COALESCE(dc.downloads, 0))', 'downloads');
    $query->addExpression('SUM(COALESCE(fm.uploads, 0))', 'uploads');
    $query->addExpression('SUM(COALESCE(nfd.blog_posts, 0))', 'blog_posts');
    $query->addExpression('SUM(COALESCE(bfd.bookmarks, 0))', 'backpack_stores');
    $query->addExpression('SUM(COALESCE(rfd.resources, 0))', 'resources');
    $query->addExpression('SUM(COALESCE(ffd.forum_topic, 0))', 'forum_topic');
    $query->addExpression('SUM(COALESCE(gct.clicks, 0))', 'clicks');
    $query->addExpression('SUM(COALESCE(nvc.views, 0))', 'views');
    $query->addExpression('SUM(COALESCE(cfd.responses, 0))', 'responses');
    $query->addJoin('LEFT', $login_select, 'lh', 'lh.uid = ufd.uid');
    $query->addJoin('LEFT', $downloads_select, 'dc', 'dc.uid = ufd.uid');
    $query->addJoin('LEFT', $uploads_select, 'fm', 'fm.uid = ufd.uid');
    $query->addJoin('LEFT', $blog_posts_select, 'nfd', 'nfd.uid = ufd.uid');
    $query->addJoin('LEFT', $bookmarks_select, 'bfd', 'bfd.uid = ufd.uid');
    $query->addJoin('LEFT', $resources_select, 'rfd', 'rfd.uid = ufd.uid');
    $query->addJoin('LEFT', $forum_topic_select, 'ffd', 'ffd.uid = ufd.uid');
    $query->addJoin('LEFT', $user_role_select, 'ur', 'ur.uid = ufd.uid');
    $query->addJoin('LEFT', $clicks_select, 'gct', 'gct.uid = ufd.uid');
    $query->addJoin('LEFT', $views_select, 'nvc', 'nvc.uid = ufd.uid');
    $query->addJoin('LEFT', $user_cohort_select, 'ufcg', 'ufcg.uid = ufd.uid');
    $query->addJoin('LEFT', $responses_select, 'cfd', 'cfd.uid = ufd.uid');
    $query->condition('ufd.uid', 0, '<>');
    $query->condition('ufd.status', 1);

    // If the role is "authenticated" no need to filter by role because all the
    // users have the authenticated role.
    if ($role != 'authenticated') {
      $query->condition('ur.roles', '%' . $this->connection->escapeLike($role) . '%', 'like');
    }

    if ($cohort_group != 0) {
      $query->condition('ufcg.cohort_group', $cohort_group, '=');
    }

    return $query;
  }

  /**
   * Returns an array of objects, each object is a Users's report row.
   *
   * @param array $options
   *   The report will be filtered using these options.
   */
  public function getUsersReport($options) {
    $start_date = $options['start_date'];
    $end_date = $options['end_date'];
    $role = $options['role'];
    $cohort_group = $options['cohort_group'];

    $login_select = $this->getLoginSelect($start_date, $end_date);
    $downloads_select = $this->getDownloadsSelect($start_date, $end_date);
    $uploads_select = $this->getUploadsSelect($start_date, $end_date);
    $blog_posts_select = $this->getBlogPostSelect($start_date, $end_date);
    $bookmarks_select = $this->getBookmarksSelect($start_date, $end_date);
    $resources_select = $this->getResourcesSelect($start_date, $end_date);
    $forum_topic_select = $this->getForumTopicSelect($start_date, $end_date);
    $clicks_select = $this->getClicksSelect($start_date, $end_date);
    $views_select = $this->getViewsSelect($start_date, $end_date);
    $responses_select = $this->getDiscussionsResponseSelect($start_date, $end_date);
    $user_role_select = $this->getUserRoleSelect();
    $user_cohort_select = $this->getCohortSelect();

    $query = $this->connection->select('users_field_data', 'ufd');
    $query->fields('ufd', ['uid', 'name']);
    $query->addField('ufcg', 'cohort_group');
    $query->addExpression('COALESCE(lh.logins, 0)', 'logins');
    $query->addExpression('COALESCE(dc.downloads, 0)', 'downloads');
    $query->addExpression('COALESCE(fm.uploads, 0)', 'uploads');
    $query->addExpression('COALESCE(nfd.blog_posts, 0)', 'blog_posts');
    $query->addExpression('COALESCE(bfd.bookmarks, 0)', 'backpack_stores');
    $query->addExpression('COALESCE(rfd.resources, 0)', 'resources');
    $query->addExpression('COALESCE(ffd.forum_topic, 0)', 'forum_topic');
    $query->addExpression('COALESCE(gct.clicks, 0)', 'clicks');
    $query->addExpression('COALESCE(nvc.views, 0)', 'views');
    $query->addExpression('COALESCE(cfd.responses, 0)', 'responses');
    $query->addJoin('LEFT', $login_select, 'lh', 'lh.uid = ufd.uid');
    $query->addJoin('LEFT', $downloads_select, 'dc', 'dc.uid = ufd.uid');
    $query->addJoin('LEFT', $blog_posts_select, 'nfd', 'nfd.uid = ufd.uid');
    $query->addJoin('LEFT', $bookmarks_select, 'bfd', 'bfd.uid = ufd.uid');
    $query->addJoin('LEFT', $resources_select, 'rfd', 'rfd.uid = ufd.uid');
    $query->addJoin('LEFT', $forum_topic_select, 'ffd', 'ffd.uid = ufd.uid');
    $query->addJoin('LEFT', $user_role_select, 'ur', 'ur.uid = ufd.uid');
    $query->addJoin('LEFT', $clicks_select, 'gct', 'gct.uid = ufd.uid');
    $query->addJoin('LEFT', $views_select, 'nvc', 'nvc.uid = ufd.uid');
    $query->addJoin('LEFT', $user_cohort_select, 'ufcg', 'ufcg.uid = ufd.uid');
    $query->addJoin('LEFT', $responses_select, 'cfd', 'cfd.uid = ufd.uid');
    $query->addJoin('LEFT', $uploads_select, 'fm', 'fm.uid = ufd.uid');
    $query->condition('ufd.uid', 0, '<>');
    $query->condition('ufd.status', 1);

    // If the role is "authenticated" no need to filter by role because all the
    // users have the authenticated role.
    if ($role != 'authenticated') {
      $query->condition('ur.roles', '%' . $this->connection->escapeLike($role) . '%', 'like');
    }

    if ($cohort_group != 0) {
      $query->condition('ufcg.cohort_group', $cohort_group, '=');
    }

    return $query;
  }

  /**
   * Returns a single object with the total of the report.
   *
   * @param $options
   *   The total row will be filtered using these options.
   */
  public function getPagesTotalRow($options) {
    $start_date = $options['start_date'];
    $end_date = $options['end_date'];
    $content_type = $options['content_type'];

    $views_select = $this->getViewsSelect($start_date, $end_date, 'nid');
    $responses_select = $this->getDiscussionsResponseSelect($start_date, $end_date, 'entity_id');
    $bookmarks_select = $this->getBookmarksSelect($start_date, $end_date, 'nid');
    $uploads_select = $this->getNodeUploadsSelect($start_date, $end_date);
    $clicks_select = $this->getClicksSelect($start_date, $end_date, 'nid');
    $downloads_select = $this->getNodeDownloadsSelect($start_date, $end_date);

    $query = $this->connection->select('node_field_data', 'nfd');
    $query->addExpression('SUM(COALESCE(nvc.views, 0))', 'views');
    $query->addExpression('SUM(COALESCE(cfd.responses, 0))', 'responses');
    $query->addExpression('SUM(COALESCE(bfd.bookmarks, 0))', 'backpack_stores');
    $query->addExpression('SUM(COALESCE(fm.uploads, 0))', 'uploads');
    $query->addExpression('SUM(COALESCE(gct.clicks, 0))', 'clicks');
    $query->addExpression('SUM(COALESCE(dc.downloads, 0))', 'downloads');
    $query->addJoin('LEFT', $views_select, 'nvc', 'nfd.nid = nvc.nid');
    $query->addJoin('LEFT', $responses_select, 'cfd', 'nfd.nid = cfd.entity_id');
    $query->addJoin('LEFT', $bookmarks_select, 'bfd', 'nfd.nid = bfd.nid');
    $query->addJoin('LEFT', $uploads_select, 'fm', 'nfd.nid = fm.entity_id');
    $query->addJoin('LEFT', $clicks_select, 'gct', 'nfd.nid = gct.nid');
    $query->addJoin('LEFT', $downloads_select, 'dc', 'nfd.nid = dc.nid');

    if ($content_type != 'all') {
      $query->condition('nfd.type', $content_type, '=');
    }

    return $query;

  }

  /**
   * Returns an array of objects, each object is a Pages's report row.
   *
   * @param array $options
   *   The report will be filtered using these options.
   */
  public function getPagesReport($options) {
    $start_date = $options['start_date'];
    $end_date = $options['end_date'];
    $content_type = $options['content_type'];

    $views_select = $this->getViewsSelect($start_date, $end_date, 'nid');
    $responses_select = $this->getDiscussionsResponseSelect($start_date, $end_date, 'entity_id');
    $bookmarks_select = $this->getBookmarksSelect($start_date, $end_date, 'nid');
    $uploads_select = $this->getNodeUploadsSelect($start_date, $end_date);
    $downloads_select = $this->getNodeDownloadsSelect($start_date, $end_date);
    $clicks_select = $this->getClicksSelect($start_date, $end_date, 'nid');

    $query = $this->connection->select('node_field_data', 'nfd');
    $query->fields('nfd', ['nid', 'title']);
    $query->addExpression('COALESCE(nvc.views, 0)', 'views');
    $query->addExpression('COALESCE(cfd.responses, 0)', 'responses');
    $query->addExpression('COALESCE(bfd.bookmarks, 0)', 'backpack_stores');
    $query->addExpression('COALESCE(fm.uploads, 0)', 'uploads');
    $query->addExpression('COALESCE(gct.clicks, 0)', 'clicks');
    $query->addExpression('COALESCE(dc.downloads, 0)', 'downloads');
    $query->addJoin('LEFT', $views_select, 'nvc', 'nfd.nid = nvc.nid');
    $query->addJoin('LEFT', $responses_select, 'cfd', 'nfd.nid = cfd.entity_id');
    $query->addJoin('LEFT', $bookmarks_select, 'bfd', 'nfd.nid = bfd.nid');
    $query->addJoin('LEFT', $uploads_select, 'fm', 'nfd.nid = fm.entity_id');
    $query->addJoin('LEFT', $clicks_select, 'gct', 'nfd.nid = gct.nid');
    $query->addJoin('LEFT', $downloads_select, 'dc', 'nfd.nid = dc.nid');

    if ($content_type != 'all') {
      $query->condition('nfd.type', $content_type, '=');
    }

    return $query;

  }

  /**
   * Returns the select object to build the login column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getLoginSelect($start_date, $end_date) {
    $login_select = $this->connection->select('login_history', 'l');
    $login_select->addField('l', 'uid');
    $login_select->addExpression('COUNT(l.login)', 'logins');
    $login_select->condition('l.login', [$start_date, $end_date], 'BETWEEN');
    $login_select->groupBy('l.uid');

    return $login_select;
  }

  /**
   * Returns the select object to build the Download column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getDownloadsSelect($start_date, $end_date) {
    $downloads_select = $this->connection->select('download_count', 'd');
    $downloads_select->addField('d', 'uid');
    $downloads_select->addExpression('COUNT(1)', 'downloads');
    $downloads_select->condition('d.timestamp', [$start_date, $end_date], 'BETWEEN');
    $downloads_select->groupBy('d.uid');

    return $downloads_select;
  }

  /**
   * Returns the select object to build the Downloads column.
   *
   * This is for create the page report.
   *
   * @param $start_date
   * @param $end_date
   */
  protected function getNodeDownloadsSelect($start_date, $end_date) {
    $downloads_select = $this->connection->select('download_count', 'd');
    $downloads_select->addField('nfa', 'entity_id', 'nid');
    $downloads_select->addExpression('COUNT(1)', 'downloads');
    $downloads_select->addJoin('LEFT', 'node__field_associated_files', 'nfa', 'd.fid = nfa.field_associated_files_target_id');
    $downloads_select->isNotNull('nfa.entity_id');
    $downloads_select->condition('d.timestamp', [$start_date, $end_date], 'BETWEEN');
    $downloads_select->groupBy('entity_id');

    return $downloads_select;
  }

  /**
   * Returns the select object to build the Uploads column.
   *
   * This is for  create the page report.
   *
   * @param $start_date
   * @param $end_date
   */
  protected function getNodeUploadsSelect($start_date, $end_date) {
    $uploads_select = $this->connection->select('file_managed', 'fm');
    $uploads_select->addField('nfa', 'entity_id');
    $uploads_select->addExpression('COUNT(1)', 'uploads');
    $uploads_select->addJoin('LEFT', 'node__field_associated_files', 'nfa', 'fm.fid = nfa.field_associated_files_target_id');
    $uploads_select->isNotNull('nfa.entity_id');
    $uploads_select->condition('fm.created', [$start_date, $end_date], 'BETWEEN');
    $uploads_select->groupBy('entity_id');

    return $uploads_select;
  }

  /**
   * Returns the select object to build the Upload column.
   *
   * @param $start_date
   * @param $end_date
   */
  protected function getUploadsSelect($start_date, $end_date) {
    $uploads_select = $this->connection->select('file_managed', 'u');
    $uploads_select->addField('u', 'uid');
    $uploads_select->addExpression('COUNT(u.created)', 'uploads');
    $uploads_select->condition('u.created', [$start_date, $end_date], 'BETWEEN');
    $uploads_select->groupBy('u.uid');

    return $uploads_select;
  }

  /**
   * Returns the select object to build the BlogPosts column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getBlogPostSelect($start_date, $end_date) {
    $blog_posts_select = $this->connection->select('node_field_data', 'nfd');
    $blog_posts_select->addField('nfd', 'uid');
    $blog_posts_select->addExpression('COUNT(nfd.type)', 'blog_posts');
    $blog_posts_select->condition('nfd.created', [$start_date, $end_date], 'BETWEEN');
    $blog_posts_select->condition('nfd.type', 'journal');
    $blog_posts_select->groupBy('nfd.uid');

    return $blog_posts_select;
  }


  /**
   * Returns the select object to build the Bookmark column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getBookmarksSelect($start_date, $end_date, $column_table = 'uid') {
    $alias = 'b';
    $bookmarks_select = $this->connection->select('bookmark_field_data', $alias);
    $bookmarks_select->addExpression('COUNT(1)', 'bookmarks');
    // There isn't a nid column in the bookmark_field_data so the uri is going
    // to be used instead.
    if ($column_table == 'nid') {
      $bookmarks_select->addExpression("TRIM(LEADING 'entity:node/' FROM url__uri)", $column_table);
      $bookmarks_select->condition('url__uri', '%' . $this->connection->escapeLike('entity:node/') . '%', 'LIKE');
    }
    else {
      $bookmarks_select->addField($alias, $column_table);
    }

    $bookmarks_select->condition($alias . '.created', [$start_date, $end_date], 'BETWEEN');
    $bookmarks_select->groupBy($column_table);

    return $bookmarks_select;
  }

  /**
   * Returns the select object to build the Resources column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getResourcesSelect($start_date, $end_date) {
    $resources_select = $this->connection->select('node_field_data', 'nfd');
    $resources_select->addField('nfd', 'uid');
    $resources_select->addExpression('COUNT(nfd.type)', 'resources');
    $resources_select->condition('nfd.created', [$start_date, $end_date], 'BETWEEN');
    $resources_select->condition('nfd.type', 'resource');
    $resources_select->groupBy('nfd.uid');

    return $resources_select;
  }

  /**
   * Returns the select object to build the Forum column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getForumTopicSelect($start_date, $end_date) {
    $forum_topic_select = $this->connection->select('node_field_data', 'nfd');
    $forum_topic_select->addField('nfd', 'uid');
    $forum_topic_select->addExpression('COUNT(nfd.type)', 'forum_topic');
    $forum_topic_select->condition('nfd.created', [$start_date, $end_date], 'BETWEEN');
    $forum_topic_select->condition('nfd.type', 'forum');
    $forum_topic_select->groupBy('nfd.uid');

    return $forum_topic_select;
  }

  /**
   * Returns the select object to build the Click through column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getClicksSelect($start_date, $end_date, $column_table = 'uid') {
    $clicks_select = $this->connection->select('guts_click_through', 'gct');
    $clicks_select->addField('gct', $column_table);
    $clicks_select->addExpression('COUNT(gct.link_path)', 'clicks');
    $clicks_select->condition('gct.created', [$start_date, $end_date], 'BETWEEN');
    $clicks_select->groupBy('gct.' . $column_table);

    // Filter all the non-node based urls if the select is using the nid
    // column.
    if ($column_table == 'nid') {
      $clicks_select->isNotNull('nid');
    }
    return $clicks_select;
  }

  /**
   * Returns the select object to build the views column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   * @param string $column_table
   *   The column returned by the query, can be uid or nid.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getViewsSelect($start_date, $end_date, $column_table = 'uid') {
    $alias = 'nvc';
    $group_by = $alias . '.' . $column_table;
    // Page Views.
    $views_select = $this->connection->select('nodeviewcount', $alias);
    $views_select->addField($alias, $column_table);
    $views_select->addExpression('COUNT(1)', 'views');
    $views_select->condition($alias . '.datetime', [$start_date, $end_date], 'BETWEEN');
    $views_select->groupBy($group_by);

    return $views_select;
  }

  /**
   * Returns the select object to build the Discussions Response Column.
   *
   * @param int $start_date
   *   The start timestamp date.
   * @param int $end_date
   *   The end timestamp date.
   *
   * @return  \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getDiscussionsResponseSelect($start_date, $end_date, $column_table = 'uid') {
    $alias = 'cfd';
    $group_by = $alias . '.' . $column_table;
    $responses_select = $this->connection->select('comment_field_data', $alias);
    $responses_select->addField($alias, $column_table);
    $responses_select->addExpression('COUNT(1)', 'responses');
    $responses_select->condition('cfd.created', [$start_date, $end_date], 'BETWEEN');
    $responses_select->groupBy($group_by);

    return $responses_select;
  }

  /**
   * Returns the select object to build the Role Column.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getUserRoleSelect() {
    $user_role_select = $this->connection->select('user__roles', 'ur');
    $user_role_select->addField('ur', 'entity_id', 'uid');
    $user_role_select->addExpression('GROUP_CONCAT(CONCAT(roles_target_id))', 'roles');
    $user_role_select->groupBy('ur.entity_id');

    return $user_role_select;
  }


  /**
   * Returns the select object to build the cohort column.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   */
  protected function getCohortSelect() {
    $user_cohort_select = $this->connection->select('user__field_cohort_group', 'ufcg');
    $user_cohort_select->addField('ufcg', 'entity_id', 'uid');
    $user_cohort_select->addField('ufcg', 'field_cohort_group_target_id', 'cohort_group');

    return $user_cohort_select;
  }
}
