<?php

namespace Drupal\guts_reports\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements PagesReportForm Form.
 */
class PagesReportForm extends FormBase {

  /**
   * The entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  public function __construct(EntityManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pages_report_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['fieldset'] = [
      '#type' => 'fieldset',
    ];

    // Try to get the values from the URL or use the default values.
    $request = $this->getRequest();
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');
    $content_type = $request->get('content_type');
    $start_date = (empty($start_date)) ? strtotime(UsersReportForm::DEFAULT_START_DATE) : strtotime($start_date);
    $end_date = (empty($end_date)) ? strtotime(UsersReportForm::DEFAULT_END_DATE) : strtotime($end_date);
    $content_type = (empty($content_type)) ? 'all' : $content_type;

    $form['fieldset']['date_container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['fieldset']['date_container']['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#default_value' => date('Y-m-d', $start_date),
    ];

    $form['fieldset']['date_container']['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#default_value' => date('Y-m-d', $end_date),
    ];

    $content = ['all' => '-- All --'];
    foreach ($this->entityManager->getStorage('node_type')->loadMultiple() as $type) {
      $access = $this->entityManager->getAccessControlHandler('node')->createAccess($type->id(), NULL, [], TRUE);
      if ($access->isAllowed()) {
        $content[$type->id()] = $type->label();
      }
    }
    $form['fieldset']['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $content,
      '#default_value' => $content_type,
    ];

    $form['fieldset']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#name' => 'submit',
    ];

    $form['fieldset']['export_to_csv'] = [
      '#type' => 'submit',
      '#value' => 'Export to CSV',
      '#name' => 'export_to_csv',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $clicked_button = $form_state->getTriggeringElement();
    $values = $form_state->getValues();
    // The form_token, form_id, and submit array won't be added in the redirect.
    $ignore_keys = array_merge(['submit'], $form_state->getCleanValueKeys());
    $query = [];
    foreach ($values as $key => $value) {
      if (in_array($key, $ignore_keys)) {
        continue;
      }
      $query[$key] = $value;
    }

    if ($clicked_button['#name'] == 'export_to_csv') {
      $query += ['format' => 'csv'];
    }

    $url = Url::fromRoute('guts_reports.pages_report', [], ['query' => $query]);
    $form_state->setRedirectUrl($url);
  }

}
