<?php

namespace Drupal\guts_reports\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;

/**
 * Implements GutsReports Form.
 */
class UsersReportForm extends FormBase {

  /**
   * Default start date value.
   */
  const DEFAULT_START_DATE = "-30 days";

  /**
   * Default end date value.
   */
  const DEFAULT_END_DATE = "tomorrow";

  /**
   * The default user's role.
   */
  const DEFAULT_ROLE = 'authenticated';

  /**
   * Default Cohort group.
   */
  const COHORT_GROUP = 0;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'guts_reports';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['fieldset'] = [
      '#type' => 'fieldset',
    ];

    // Try to get the values from the URL or use the default values.
    $request = $this->getRequest();
    $start_date = $request->get('start_date');
    $end_date = $request->get('end_date');
    $role = $request->get('role');
    $cohort_group = $request->get('cohort_group');
    $start_date = (empty($start_date)) ? strtotime(UsersReportForm::DEFAULT_START_DATE) : strtotime($start_date);
    $end_date = (empty($end_date)) ? strtotime(UsersReportForm::DEFAULT_END_DATE) : strtotime($end_date);
    $role = (empty($role)) ? UsersReportForm::DEFAULT_ROLE : $role;
    $cohort_group = (empty($cohort_group)) ? UsersReportForm::COHORT_GROUP : $cohort_group;

    $roles = user_roles(TRUE);
    $roles_list = [];
    foreach ($roles as $item) {
      $roles_list[$item->id()] = $item->label();
    }
    $form['fieldset']['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#options' => $roles_list,
      '#default_value' => $role,
    ];

    $form['fieldset']['date_container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['container-inline']],
    ];

    $form['fieldset']['date_container']['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#default_value' => date('Y-m-d', $start_date),
    ];

    $form['fieldset']['date_container']['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#default_value' => date('Y-m-d', $end_date),
    ];

    $vocabulary_name = 'cohort_group';
    $query = \Drupal::entityQuery('taxonomy_term')->accessCheck(TRUE);
    $query->condition('vid', $vocabulary_name);
    $query->sort('weight');
    $tids = $query->execute();
    $terms = Term::loadMultiple($tids);
    $terms_list = [0 => '-- None --'];
    foreach ($terms as $term) {
      $terms_list[$term->id()] = $term->label();
    }
    $form['fieldset']['cohort_group'] = [
      '#title' => $this->t('Cohort group'),
      '#type' => 'select',
      '#options' => $terms_list,
      '#default_value' => $cohort_group,
    ];

    $form['fieldset']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#name' => 'submit',
    ];

    $form['fieldset']['export_to_csv'] = [
      '#type' => 'submit',
      '#value' => 'Export to CSV',
      '#name' => 'export_to_csv',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $clicked_button = $form_state->getTriggeringElement();
    $values = $form_state->getValues();
    // The form_token, form_id, and submit array won't be added in the redirect.
    $ignore_keys = array_merge(['submit'], $form_state->getCleanValueKeys());
    $query = [];
    foreach ($values as $key => $value) {
      if (in_array($key, $ignore_keys)) {
        continue;
      }
      $query[$key] = $value;
    }

    if ($clicked_button['#name'] == 'export_to_csv') {
      $query += ['format' => 'csv'];
    }

    $url = Url::fromRoute('guts_reports.users_report', [], ['query' => $query]);
    $form_state->setRedirectUrl($url);
  }

}
