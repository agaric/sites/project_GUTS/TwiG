<?php

namespace Drupal\featherlight\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to resize images.
 *
 * @Filter(
 *   id = "filter_image_featherbox",
 *   title = @Translation("Image Featherbox effect filter"),
 *   description = @Translation("Displays the images on a lightbox when clicked."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class FeatherLight extends FilterBase implements ContainerFactoryPluginInterface {


  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);

    /** @var \DOMNode $node */
    foreach ($xpath->query('//img') as $node) {
      $src = $node->getAttribute('src');

      /** @var \DOMNode $element */
      $element = $dom->createElement('a');
      $element->setAttribute('data-featherlight', $src);
      $element->setAttribute('href', '#');
      $node->parentNode->replaceChild($element, $node);
      $element->appendChild($node);
    }
    $result->setProcessedText(Html::serialize($dom));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

}