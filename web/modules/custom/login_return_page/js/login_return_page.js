(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.linkReturnPage = {
    attach: function (context) {
      var current_page = location.pathname,
        login_page = '/user/login';

      if (login_page != current_page) {
        if (current_page == '/members/map') {
          current_page = '/members/search';
        }

        $(once('login-processed-link', (context).find("a[href='" + login_page + "']"))).each(function () {
          $(this).attr('href', $(this).attr('href') + '?destination=' + current_page);
        });
      }
    }
  }
})(jQuery, Drupal);
