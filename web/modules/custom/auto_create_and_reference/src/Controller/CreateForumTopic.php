<?php

namespace Drupal\auto_create_and_reference\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;

class CreateForumTopic extends ControllerBase {
  /**
   * Check if the topic related with the Resource/News has been already created.
   */
  public function content(NodeInterface $node) {
    $query = \Drupal::entityQuery('node')->accessCheck(TRUE);
    $query->condition('type', 'forum');
    $query->condition('field_resource', $node->id());
    $entity_ids = $query->execute();

    // If the topic doesn't exists, lets create it:
    if (!$entity_ids) {
      // If this discussions comes from a resource the Forum must be in the 'Contributed Resources' category.
      if ($node->getType() == 'resource') {
        $taxonomy_term = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => 'Contributed Resources']);
      } else if ($node->getType() == 'news') {
        // If the discussion comes from a news the Forum topic must be in the 'News discussions' category.
        $taxonomy_term = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['name' => 'News discussions']);;
      }
      $taxonomy_term = (!empty($taxonomy_term)) ? array_pop($taxonomy_term) : null;
      $created_entity = Node::create([
        'nid' => NULL,
        'type' => 'forum',
        'title' => $node->label(),
        'status' => NODE_PUBLISHED,
        'uid' => $node->getOwnerId(),
        'taxonomy_forums' => ($taxonomy_term) ? $taxonomy_term->id() : null,
        'field_resource' => ['target_id' => $node->id()],
        'field_tags' => node_load($node->id())->get('field_tag')->getValue(),
      ]);
      $created_entity->save();
      $topic_id = $created_entity->id();
    } else {
      $topic_id = array_pop($entity_ids);
    }

    // Redirect to the topic.
    return $this->redirect('entity.node.canonical', ['node' => $topic_id]);
  }
}
