<?php

namespace Drupal\guts_form_clean_up\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'file_hypothesis' formatter.
 *
 * @FieldFormatter(
 *   id = "file_hypothesis_link",
 *   label = @Translation("Hypothesis Link"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class HypothesisFileLink extends FormatterBase {

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * The field values to be rendered.
   * @param string $langcode
   * The language that should be used to render the field.
   *
   * @return array
   * A renderable array for $items, as an array of child elements keyed by
   * consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    $field_name = $items->getFieldDefinition()->getName();
    if ($field_name == 'field_curriculum') {
      $label = 'Curriculum';
    } else {
      $label = 'Pacing Guide';
    }

    // Loop through items
    foreach ($items as $delta => $fieldItem) {
      /** @var \Drupal\file_entity\Entity\FileEntity $file */
      $file = $fieldItem->entity;
      $uri = $file->getFileUri();
      $filename = $file->getFilename();
      $mime = $file->getMimeType();
      $size = $file->getSize();
      $elements[$delta] = array(
        '#theme' => 'hypothesis_link',
        '#label' => $label,
        '#uri' => $uri,
        '#filename' => $filename,
        '#filemime' => $mime,
        '#filesize' => $size,
      );
    }
    return $elements;
  }
}