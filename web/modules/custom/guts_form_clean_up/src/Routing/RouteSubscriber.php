<?php

namespace Drupal\guts_form_clean_up\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    // The webook_endpoint must be usable by anonymous users.
    if ($route = $collection->get('mailchimp.webhook_endpoint')) {
      $route->setRequirements(['_access' => 'TRUE']);
    }
  }

}
