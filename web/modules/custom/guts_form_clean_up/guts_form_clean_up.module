<?php
use Drupal\Core\Render\Element;
use Drupal\Core\Access\AccessResult;
/**
 * Implements hook_form_alter().
 */
function guts_form_clean_up_form_alter(
    &$form,
    \Drupal\Core\Form\FormStateInterface $form_state,
    $form_id
) {

  // Enforce the maxlength limit in the textareas.
  $form['#validate'][] = 'guts_form_clean_up_form_validate';

  if (in_array($form_id, ['node_resource_form', 'node_resource_edit_form'])) {
    // Removing the <br/> in the field_image description and in the
    // field_associated_files description.
    $info = (string) $form['field_image']['widget'][0]['#description'];
    $description = explode("<br />", strip_tags($info, '<br>'));

    // The description is divided in 4 parts (when the field is empty).
    // <the description> <number of files> <file size> <allowed types>
    // and in only one part with is not empty, so we will get the description
    // only when the field is empty and use it as a title.
    if (count($description) == 4 || count($description) == 1) {
      $form['field_image']['widget'][0]['#label_suffix'] = t('<span class="small-text">@description</span>', ['@description' => $description[0]]);
      unset($description[0]);
      $info =  implode("<br />", $description);
    }
    $form['field_image']['widget'][0]['#description'] = t(str_replace("<br />","&nbsp;", $info));

    // the Title hasn't description, se we are going to add it.
    $form['title']['widget'][0]['value']['#description'] = 'Choose a short, descriptive title.';
    $form['title']['widget'][0]['#description'] = 'Choose a short, descriptive title.';

    // Removing all the descriptions of the links fields.
    foreach (Element::children($form['field_links']['widget']) as $key) {
      $form['field_links']['widget'][$key]['uri']['#description'] = '';
    }

    // See the function hide_description_description/
    foreach (Element::children($form['field_associated_files']['widget']) as $key) {
      $form['field_associated_files']['widget'][$key]['#process'][] = 'hide_description_description';
    }

    // Change the "Add another item" to "Add another" in the links submit
    // button.
    $form['field_links']['widget']['add_more']['#value'] = t('Add another');
  }
}

/**
 * Enforce to the limit is not exceeded in the text areas with the maxlength
 * attribute.
 */
function guts_form_clean_up_form_validate($form, &$form_state) {
  foreach (Element::children($form) as $key) {
    if (isset($form[$key]['widget'][0]['#maxlength_js']) && $form[$key]['widget'][0]['#maxlength_js']) {
      $maxlenght = isset($form[$key]['widget'][0]['#attributes']['maxlength']) ? $form[$key]['widget'][0]['#attributes']['maxlength'] : 500;
      $label = isset($form[$key]['widget'][0]['#title']) ? $form[$key]['widget'][0]['#title'] : 'Field';

      if (strlen($form_state->getValue($key)[0]['value']) > $maxlenght) {
        $form_state->setErrorByName('field_summary', t("The {$label} cannot have more than {$maxlenght} characters."));
      }
    }
  }
}

/**
 * Removes all the descriptions of the resource files fields.
 * this must be done in a #process callback because is there where the
 * description field is built.
 */
function hide_description_description($element, \Drupal\Core\Form\FormState $form_state, $form) {
  $element['description']['#description'] = 'Provide a title that will be used instead of the file name for the link to download the file. If left blank, the file name will be used.';
  $element['description']['#title'] = t('Title');
  return $element;
}

/**
 * Implements hook_block_access().
 *
 */
function guts_form_clean_up_block_access(\Drupal\block\Entity\Block $block, $operation, \Drupal\Core\Session\AccountInterface $account) {

  // The block "Member posts" shouldn't be displayed in the user edit form.
  $route = \Drupal::routeMatch()->getRouteName();
  if ($operation == 'view' && $block->getPluginId() == 'views_block:member_posts-block') {
    return AccessResult::forbiddenIf($route == 'entity.user.edit_form')->addCacheableDependency($block);
  }
  return AccessResult::neutral();
}
