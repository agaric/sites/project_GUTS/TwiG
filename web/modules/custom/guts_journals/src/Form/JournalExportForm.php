<?php

namespace Drupal\guts_journals\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Implements JournalExportForm Form.
 */
class JournalExportForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * JournalExportForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'journal_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['export'] = [
      '#type' => 'submit',
      '#value' => 'Export',
      '#attributes' => [
        'class' => ['btn', 'btn-resources-search', 'btn-block'],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parameters = $this->getRouteMatch()->getParameters();
    $user_id = $parameters->get('arg_0');

    if (!$user_id) {
      return;
    }

    $entity_query = $this->entityTypeManager->getStorage('node')->accessCheck(TRUE)->getQuery('AND');
    $entries = $entity_query->condition('uid', $user_id)
      ->condition('status', 1)
      ->condition('type', 'journal')
      ->execute();

    $user = $this->entityTypeManager->getStorage('user')->load($user_id);

    $title = [
      'Date',
      'Title',
      'Body',
    ];

    $temp = file_directory_temp() . '/' . $user->label() . '_journal_entries.csv';
    $f = fopen($temp, 'w');
    fputcsv($f, $title, ',');

    foreach ($entries as $entry) {
      /** @var \Drupal\node\NodeInterface $journal_entry */
      $journal_entry = $this->entityTypeManager->getStorage('node')->load($entry);

      fputcsv($f, [
        date('Y-m-d H:i:s', $journal_entry->getCreatedTime()),
        $journal_entry->label(),
        $journal_entry->get('body')->value,
      ], ',');
    }
    fclose($f);

    $form_state->setResponse(new BinaryFileResponse($temp, 200, [], TRUE, 'attachment'));
  }
}
