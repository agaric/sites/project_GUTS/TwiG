Taxonomy redirect for Drupal 8
-------------------------------------------

Redirects taxonomy term links to a custom route.

_NOTE_

The plan is to provide an interface for setting an arbitrary number of custom
paths, or routes, based on vocabulary.  Starting with hard-coded as a
demonstration.

### Installation

Install per normal https://www.drupal.org/documentation/install/modules-themes/modules-8.

### Credits

Initial development and maintenance by [Agaric](http://agaric.com/).
