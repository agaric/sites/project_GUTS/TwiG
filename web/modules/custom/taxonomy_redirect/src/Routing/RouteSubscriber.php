<?php

/**
 * @file
 * Contains \Drupal\taxonomy_redirect\Routing\RouteSubscriber.
 */

namespace Drupal\taxonomy_redirect\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.taxonomy_term.canonical')) {
      $route->setPath('/taxonomy_redirect/term/{taxonomy_term}');
      $route->setDefault('_controller', '\Drupal\taxonomy_redirect\Controller\TaxonomyRedirectController::taxonomyRedirect');
      $route->setDefault('_title', 'Taxonomy term redirect');
      // $route->setRequirement('_access', 'TRUE');
      // $route->setDefault('_entity_view', NULL);
      $route->setDefault('_title_callback', NULL);
      // $route->setRequirements(array('_access', 'TRUE'));
    }
  }

}
