<?php

/**
 * @file
 * Handles counts of clicks via AJAX with minimal bootstrap.
 *
 * This doesn't work unless this script be at the same level as the drupal
 * web root, it might be something related with:
 * https://drupal.stackexchange.com/questions/224104/php-access-user-data-from-external-script#comment273223_224111
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

chdir('../../..');

$autoloader = require_once 'autoload.php';

$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();
$container = $kernel->getContainer();

$click_path = filter_input(INPUT_POST, 'click_path', FILTER_SANITIZE_URL);
$uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_URL);

if ($click_path) {
  $path = $container->get('path_alias.manager')->getPathByAlias($click_path);
  $nid = null;
  if(preg_match('/node\/(\d+)/', $path, $matches)) {
    $nid = $matches[1];
  }

  $connection = $container->get('database');
  $connection->insert('guts_click_through')
    ->fields([
      'uid' => $uid,
      'link_path' => $click_path,
      'created' => time(),
      'nid' => $nid,
    ])
    ->execute();
}
