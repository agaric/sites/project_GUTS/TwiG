(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.gutsClickThrough = {
    attach: function (context, settings) {
      $('a', context).once('guts-click-through').each(function () {
        $(this).click(function(e) {
          var href = $(this).attr("href");
          if (href === "#" || !href) {
            return false;
          }
          $.ajax({
            type: 'POST',
            cache: false,
            url: '/modules/custom/guts_click_through/guts_click_through.php',
            data: {
              click_path: href,
              uid: drupalSettings.user.uid
            }
          });
        });
      });
    }
  };

})(jQuery, Drupal);
