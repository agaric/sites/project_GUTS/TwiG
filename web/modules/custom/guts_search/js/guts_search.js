(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.gutsSearch = {
    attach: function (context) {
      var $search_form = $('#sitewide-search-block-form'),
          $select = $search_form.find("select[data-drupal-selector='edit-type']"),
          $input = $search_form.find("input[data-drupal-selector='edit-keyword']"),
          $expand_search_link = $('#expand-search'),
          $search_contact_form = $('#block-contactblock'),
          action_mapping = {
            "content_users": '/search-any',
            "resource": '/resources/search',
            "member": '/members/search',
            "forum": '/discussions/search',
            "news": '/news/search',
            "page": '/help/search',
            "faq": '/faq/search',
            "upcoming_event": '/events/upcoming',
            'past_event': '/events/past'
          },
          update_action = function(select) {
            var type = $( select ).val();
            if (type in action_mapping) {
              $search_form.attr('action', action_mapping[type]);
            }
          },
          update_search = function(search) {
            $('#edit-field-search-0-value').val(search);
          },
          is_last_page = function() {
            // Check if we are in the search page.
            if ($('#block-contactblock').length === 0) {
              return false;
            }

            // Check if we are in the last page, we do that checking if the
            // last link exists in the pager, usually this link is not printed
            // when the user is on the last page.
            return ($('.pager__item--last').length === 0)
          };

      // The dropdown must remember what the user is searching.
      var path = window.location.pathname;
      var search = window.location.href.match(/\?keyword=([\w\+]+)/);
      if (search) {
        search[1] = search[1].replace(/\+/g, " ");
      }

      $.each(action_mapping, function (index, element) {
        if (element === path) {
          $select.val(index);
        }
        if (search != null) {
          $input.val(search[1]);
        }
      });

      update_action($select);
      $select.change(function () {
        update_action(this);
      });

      // Expand search link.
      if ($expand_search_link.length > 0 && search != null && is_last_page()) {
        var text = $expand_search_link.text(),
        updated_text = text.replace("[keyword]", search[1]);
        $expand_search_link.text(updated_text);
        $expand_search_link.attr('href', '/search-any?keyword=' + search[1])
      } else {
        // If the search is empty, let's hide the block.
        $('.expand-search-wrapper').hide();
      }

      // Search contact form.
      if ($search_contact_form.length > 0 && search != null && is_last_page()) {
        update_search(search[1] + " In: " + $select.val());
        $('input', $search_form).on('keyup', function() {
          if (this.value.length > 1) {
            update_search(this.value + " In: " + $select.val());
          }
        });
        $select.on('change', function() {
          update_search( $('input', $search_form).val() + " In:" + $select.val());
        });
      } else {
        $search_contact_form.hide();
      }

    }
  };

  // Display a loader if a facet checkbox is clicked.
  Drupal.behaviors.gutsFacetsCheckboxes = {
    attach: function (context) {
      $( document ).ready(function() {
        var $facet_block = $('.block-facet--checkbox');
        if ($facet_block.length > 0) {
          $('.facets-checkbox', context).on('click', function(event) {
            $('body').append('<div class="ajax-progress-big ajax-progress ajax-progress-fullscreen">&nbsp;</div>');
            $('.facets-checkbox').not(this).attr('disabled', 'disabled');
          });
        }
      });
    }
  }

  // Hide the facets in the views_send page if the user is not in the first page
  Drupal.behaviors.gutsHideFacets = {
    attach: function (context) {
       var $form = $('form#views-form-send-message-page-1', context);
       if ($form.length === 0) {
          return;
       }
       // If the form is displayed, let's hide the facets because the user
       // already selected to which users the message will be sent.
       if ($('.views-form').length > 0) {
          $('.block-facets').hide();
       }

    }
  }

})(jQuery, Drupal);
