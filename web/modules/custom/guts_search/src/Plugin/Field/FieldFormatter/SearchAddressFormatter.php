<?php

namespace Drupal\guts_search\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'GUTS Search Api' formatter.
 *
 * Print more data so it can be indexed via search api.
 *
 *
 * @FieldFormatter(
 *   id = "search_address",
 *   label = @Translation("GUTS Search API"),
 *   field_types = {
 *     "address"
 *   }
 * )
 */
class SearchAddressFormatter extends FormatterBase {

  /**
   * The subdivision repository.
   *
   * @var \CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface
   */
  protected $subdivisionRepository;

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [t('Displays All the data to be indexed in the search API')];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $this->subdivisionRepository = \Drupal::service('address.subdivision_repository');

    foreach ($items as $delta => $item) {
      $value = $item->getValue();
      $subdivisions = $this->subdivisionRepository->getAll([$value['country_code']]);
      $administrative_area_name = !empty($subdivisions[$value['administrative_area']]) ? $subdivisions[$value['administrative_area']]->getName() : NULL;
      $address = array();
      $address[] =
      $address[] = !empty($value['address_line1']) ? $value['address_line1'] : NULL;
      $address[] = !empty($value['address_line2']) ? $value['address_line2'] : NULL;
      $address[] = !empty($value['postal_code']) ? $value['postal_code'] : NULL;
      $address[] = !empty($value['locality']) ? $value['locality'] : NULL;
      $address[] = !empty($value['administrative_area']) ? $value['administrative_area'] : NULL;
      $address[] = !empty($administrative_area_name) ? $administrative_area_name : NULL;
      $address[] = !empty($value['country']) ? $value['country'] : NULL;

      $elements[$delta] = [
        '#markup' => "<h1>" . implode(' , ', array_filter($address)) . "</h1>",
      ];

    }

    return $elements;
  }

}
