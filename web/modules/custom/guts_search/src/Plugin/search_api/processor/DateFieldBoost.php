<?php

namespace Drupal\guts_search\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\ResultSetInterface;

/**
 * @SearchApiProcessor(
 *   id = "guts_search_date_field_boost",
 *   label = @Translation("Guts date field boost"),
 *   description = @Translation("Increment the relevance of the items depending
 *   how new they are"), stages = {
 *     "postprocess_query" = 0
 *   }
 * )
 */
class DateFieldBoost extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(ResultSetInterface $results) {
    // Depending how recent the items are they will have more or less relevance.
    // if the relevance is for instance 4 and the months limit is 24 (two years)
    // the nodes with less than a month old will have a relevance of:
    // $relevance * $months_limit =  92;
    // From there the next month will have the result of the previous relevance
    // minus  the current relevance unit, eg:
    //
    // $month | $previous_relevant_number - $relevance | New Relevance |
    // ------------------------------------------------|---------------|
    //     2  |                92 - 4                  |     88        |
    //     3  |                88 - 4                  |     84        |
    //     4  |                84 - 4                  |     80        |
    //                           ...
    //     24 |                 4 - 4                  |      0        |
    // -----------------------------------------------------------------
    $relevance = \Drupal::state()->get('date_field_boost_unit', 4);
    $months_limit = \Drupal::state()->get('date_field_boost_months', 36);

    $score_factor = [];
    $months = range(1, $months_limit);
    $current_relevance = $months_limit * $relevance;
    foreach ($months as $month_number) {
      $score_factor[$month_number] = $current_relevance;
      $current_relevance -= $relevance;
    }

    $items = [];
    /** @var \Drupal\search_api\Item\Item $item */
    foreach ($results as $key => $item) {
      $entity_type_id = $item->getDatasource()->getEntityTypeId();
      // It only works for nodes.
      if ($entity_type_id != 'node') {
        continue;
      }
      $node = $item->getOriginalObject()->getValue();
      foreach ($score_factor as $time => $score) {
        if ($node->get('created')->value >= strtotime('-' . $time . ' months')) {
          $item->setScore($item->getScore() + $score);
          break;
        }
      }

      $items[$key] = $item;
    }
    uasort($items, [$this, 'orderByScore']);
    $results->setResultItems($items);
  }

  /**
   * Order the items by score.
   *
   * @param \Drupal\search_api\Item\Item $item1
   *   The first item that is going ot be ordered.
   * @param \Drupal\search_api\Item\Item $item2
   *   The second item that is going to be ordered.
   *
   * @return int
   *   0 if it is equal, -1 if item  1 is bigger than item 2.
   */
  protected function orderByScore($item1, $item2) {
    if ($item1->getScore() == $item2->getScore()) {
      return 0;
    }
    return ($item1->getScore() > $item2->getScore()) ? -1 : 1;
  }

}
