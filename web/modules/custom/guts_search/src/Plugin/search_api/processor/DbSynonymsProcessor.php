<?php

namespace Drupal\guts_search\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Query\QueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * @SearchApiProcessor(
 *   id = "guts_search_synonym_db_processor",
 *   label = @Translation("Guts Search API Synonym DB"),
 *   description = @Translation("Check to enable synonym and spelling error integration to database search."),
 *   stages = {
 *     "preprocess_query" = 99
 *   }
 * )
 */
class DbSynonymsProcessor extends ProcessorPluginBase {
  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function preprocessSearchQuery(QueryInterface $query) {
    // Get the current keys
    $keys = $query->getKeys();

    $items = [];
    if (empty($keys)) {
      return;
    }

    foreach ($keys as $key => $string) {
      if ($key[0] !="#") {
        $items[] = trim(trim(urldecode($string), '\"\''));
      }
    }

    // Stop if the key is empty.
    if (empty($items)) {
      return ;
    }

    $result = [];
    // Find synonyms in the current keys.
    foreach ($items as $item) {
      $entity_query = $this->entityTypeManager->getStorage('search_api_synonym')->getQuery()
        ->condition('synonyms', $item)
        ->condition( 'type', 'synonym')
        ->condition('status', 1)
        ->accessCheck();
      $ids = $entity_query->execute();
      // Loop through all returned ids, check access and alter the search query.
      foreach ($ids as $id) {
        $entity_storage = $this->entityTypeManager()->getStorage('search_api_synonym');
        /** @var \Drupal\search_api_synonym\Entity\Synonym $synonym */
        $synonym = $entity_storage->load($id);
        // Check access
        if ($synonym->access('view')) {
          $result[] = $synonym->getWord();
        }
      }
    }

    if (!empty($result)) {
      $result['#conjunction'] = 'AND';
      $query->keys($result);
    }
  }
}
