<?php

namespace Drupal\guts_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'LabelBlock' block.
 *
 * @Block(
 *  id = "label_block",
 *  admin_label = @Translation("Empty label-only block for titles"),
 * )
 */
class LabelBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#markup' => '',
    ];
  }

}
