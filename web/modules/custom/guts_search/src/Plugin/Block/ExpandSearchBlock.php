<?php

namespace Drupal\guts_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ExpandSearchBlock' block.
 *
 * @Block(
 *  id = "expand_search_block",
 *  admin_label = @Translation("Expand search block"),
 * )
 */
class ExpandSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'markup',
      '#markup' => '<div class="well expand-search-wrapper"><a href="#" id="expand-search">Search for "[keyword]" across the whole site.</a></div>',
    ];
  }

}
