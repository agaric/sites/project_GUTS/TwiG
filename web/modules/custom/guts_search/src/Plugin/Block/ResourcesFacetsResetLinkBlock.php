<?php

namespace Drupal\guts_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block\Entity\Block;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a block to reset the preferences of the resources facets.
 *
 * @Block(
 *   id = "resource_reset_facets_block",
 *   admin_label = @Translation("Resource Reset Facets block"),
 *   category = @Translation("GUTS")
 * )
 */
class ResourcesFacetsResetLinkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Include a reset button.
    $reset_url = Url::fromUri('internal:/resources/search', ['attributes' => ['class' => ['btn', 'btn-resources-search', 'btn-sm']]]);
    $reset_link = Link::fromTextAndUrl('Reset', $reset_url);

    return $reset_link->toRenderable();
  }
}

