<?php

namespace Drupal\guts_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SitewideSearchBlock' block.
 *
 * @Block(
 *  id = "sitewide_search_block",
 *  admin_label = @Translation("Sitewide search block"),
 * )
 */
class SitewideSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\guts_search\Form\SitewideSearchBlockForm');
  }

}
