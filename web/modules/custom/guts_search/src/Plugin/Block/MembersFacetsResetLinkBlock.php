<?php

namespace Drupal\guts_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block\Entity\Block;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a block with a reset link for the facets of the Members page.
 *
 * @Block(
 *   id = "members_reset_facets_block",
 *   admin_label = @Translation("Members Reset Facets block"),
 *   category = @Translation("GUTS")
 * )
 */
class MembersFacetsResetLinkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Include a reset button.
    $reset_url = Url::fromUri('internal:/members/search', ['attributes' => ['class' => ['btn', 'btn-sm']]]);
    $reset_link = Link::fromTextAndUrl('Reset', $reset_url);

    return $reset_link->toRenderable();
  }
}
