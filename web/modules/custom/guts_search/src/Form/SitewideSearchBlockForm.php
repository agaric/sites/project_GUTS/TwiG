<?php

namespace Drupal\guts_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SitewideSearchBlockForm.
 *
 * @package Drupal\guts_search\Form
 */
class SitewideSearchBlockForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sitewide_search_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $searchOptions = [];

    $searchOptions['content_users'] = '- All -';

    if (!\Drupal::currentUser()->isAnonymous()) {
      $searchOptions['member'] = 'Members';
    }

    $content_types = \Drupal::service('entity_type.manager')->getStorage('node_type')->loadMultiple();
    $correct_labels = [
      'Event' => 'Events',
      'Forum topic' => 'Discussions',
      'Resource' => 'Resources',
    ];

    foreach ($content_types as $content_type) {
      $hidden_options = [
        'Curriculum',
        'Event',
        'Journal',
        'Help',
      ];

      if (in_array($content_type->label(), $hidden_options)) {
        continue;
      }

      $searchOptions[$content_type->id()] = (isset($correct_labels[$content_type->label()])) ? $correct_labels[$content_type->label()] : $content_type->label();
    }
    // Adding the events search options.
    $searchOptions['upcoming_event'] = 'Upcoming events';
    $searchOptions['past_event'] = 'Past events';

    $form['keyword'] = array(
      '#type' => 'search',
      '#title' => $this->t('Search'),
      '#title_display' => 'invisible',
      '#size' => 15,
      '#default_value' => '',
      '#attributes' => array('title' => $this->t('Enter the terms you wish to search for.')),
    );

    $form['type'] = array(
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#title_display' => 'invisible',
      '#options' => $searchOptions,
      // Prevent type from showing up in the query string.
      '#name' => '',
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Go'),
      // Prevent op from showing up in the query string.
      '#name' => '',
    );
    $form['#action'] = $GLOBALS['base_path'] . \Drupal\Core\Url::fromRoute('view.search_all_content_and_users.page_1')->getInternalPath();
    $form['#method'] = 'get';

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This form submits to the search page, so processing happens there.
  }

}
