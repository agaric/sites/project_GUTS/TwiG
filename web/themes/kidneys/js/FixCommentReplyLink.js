(function ($, Drupal, CKEDITOR) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      $(context).find('a.reply-comment-link').once('flat-reply-link').each(function () {
        $(this).on('click', function(e) {
          author_name = $(this).data('author-name');
          comment_permalink = $(this).data('permalink');
          ckeditor_name = $('#comment-form textarea', context).attr('id');
          link = '<p><em><a href="' + comment_permalink + '">In reply to ' + author_name + ': </a></em></p><br />';
          CKEDITOR.instances[ckeditor_name].setData(link);
        });
      });
    }
  };
})(jQuery, Drupal, CKEDITOR);
