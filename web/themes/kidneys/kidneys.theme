<?php

/**
 * @file
 * Functions to support theming in the Bootstrap endTB theme
 */

use Drupal\block\Entity\Block;
use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\file\Entity\File;

/**
 * Implements hook_preprocess_form().
 */
function kidneys_preprocess_form(&$variables) {
  if ($variables['element']['#form_id'] === 'search_block_form' || $variables['element']['#form_id'] === 'sitewide_search_block_form') {
    $variables['attributes']['class'] = array('navbar-form');
  }
  elseif ($variables['element']['#form_id'] === 'mailchimp_signup_subscribe_block_endtb_newsletter_form') {
    $variables['attributes']['class'] = array('mailchimp-signup-subscribe-form');
  }
}

/**
 * Implements hook_preprocess_input().
 */
function kidneys_preprocess_input(&$variables) {
  if ($variables['element']['#type'] == 'submit') {
    $variables['attributes']['class'][] = 'btn';
    $variables['attributes']['class'][] = 'btn-default';
  }
  else if(!in_array($variables['element']['#type'], ['radio', 'checkbox'])) {
      $variables['attributes']['class'][] = 'form-control';
  }
}

/**
 * Extends template_preprocess_menu_local_action().
 *
 * Add Bootstrap button classes to a single local action link and change the
 * text to use the Chicago style case.
 *
 * Default template: menu-local-action.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element containing:
 *     - #link: A menu link array with 'title', 'url', and (optionally)
 *       'localized_options' keys.
 */
function kidneys_preprocess_menu_local_action(&$variables) {
  $variables['link']['#options']['attributes']['class'][] = 'btn';
  $variables['link']['#options']['attributes']['class'][] = 'btn-success';
  $variables['link']['#title'] = guts_chicago_case_style((string) $variables['link']['#title']);
}

/**
 * Change the text to use the chicago style case.
 */
function kidneys_preprocess_links(&$variables) {
  foreach ($variables['links'] as $name => $link) {
    if (!isset($link['link'])) {
      continue;
    }
    $text = guts_chicago_case_style((string) $link['link']['#title']);
    $variables['links'][$name]['link']['#title'] = t($text);
  }
}

/**
 * Implements hook_preprocess_menu().
 */
function kidneys_preprocess_menu(&$variables) {
  if (!isset($variables['menu_name'])) {
    // We don't care about unnamed menus (toolbar etc).
    return;
  }
  $variables['attributes']['class'][] = 'menu';
  $variables['attributes']['class'][] = 'menu-' . Html::cleanCssIdentifier($variables['menu_name']);

  if ($variables['menu_name'] == 'main') {
    $variables['attributes']['class'][] = 'nav';
    $variables['attributes']['class'][] = 'navbar-nav';
  }

  if ($variables['menu_name'] == 'main' || $variables['menu_name'] == 'account' || $variables['menu_name'] == 'visitor-menu') {
    kidneys_add_ids_to_menu_items($variables['items']);
  }
}

/**
 * Add a unique ID to each menu item for adding icons with CSS.
 */
function kidneys_add_ids_to_menu_items(&$items, $id_suffix = '') {
  foreach ($items as &$item) {
    $id = \Drupal\Component\Utility\Html::getId($item['title']);
    $attributes = $item['url']->getOption('attributes');
    $attributes['id'] = 'menu-link-icon-' . $id . $id_suffix;
    $attributes['class'][] = 'menu-link-icon';
    $item['url']->setOption('attributes', $attributes);
  }
}

/**
 * Implements hook_preprocess_block().
 */
function kidneys_preprocess_block(&$variables) {

  // Add the site's base path as a variable available to block templates.
  $variables['base_path'] = base_path();

  // Add a class to blocks based on region machine name.
  if (!empty($variables['elements']['#id'])) {
    $block = Block::load($variables['elements']['#id']);
    $variables['attributes']['class'][] = 'block--' . $block->getRegion();
  }

  // Add a class to views-provided blocks to make them fit together as one.
  if ($variables['base_plugin_id'] == 'facet_block') {
    $variables['attributes']['class'][] = 'block--together';
  }

  // Add block classes used in layout.
  $block = \Drupal::entityTypeManager()->getStorage('block')->load($variables['elements']['#id']);
  $blocksPerRegion = \Drupal::service('block.repository')->getVisibleBlocksPerRegion();

  switch ($variables['plugin_id']) {
    case 'system_main_block':
      if (!empty($blocksPerRegion['content_bottom'])) {
        $variables['attributes']['class'][] = 'l-content-top';
      }
      break;
    case 'system_menu_block:main':
    case 'search_form_block':
    case 'sitewide_search_block_form':
      $variables['attributes']['class'][] = 'navbar-left';
      break;
  }
}

/**
 * Implements hook_preprocess_views_view_list().
 */
function kidneys_preprocess_views_view_list(&$variables) {
  $variables['list']['attributes'] = new Attribute();
  if (isset($variables['view']) && isset($variables['view']->build_info['title'])) {
    $variables['list']['attributes']->addClass('list-' . Html::cleanCssIdentifier(strtolower($variables['view']->build_info['title'])));
  }
}

/**
 * Implements hook_preprocess_links().
 */
function kidneys_preprocess_links__node(&$variables) {
  $variables['attributes']['class'][] = 'list-unstyled';
  $variables['attributes']['class'][] = 'links--node';
}

/**
 * Implements hook_preprocess_node().
 */
function kidneys_preprocess_node(&$variables) {
  $variables['date'] = \Drupal::service('date.formatter')->format($variables['node']->getCreatedTime(), 'no_time_short_date');
}

/**
 * Set a default values in the content types send in the mail digest.
 */
function kidneys_node_digest_variables(&$variables) {
  $options = ['absolute' => TRUE];
  // The url must be absolute.
  if (isset($variables['url'])) {
    $url = Url::fromRoute('entity.node.canonical', ['node' => $variables['node']->id()], $options);
    $url = $url->toString();
    $variables['url'] = $url;
  }
}

/**
 * Implements hook_preprocess_HOOK() for node--resource--digest.twig.html
 */
function kidneys_preprocess_node__resource__digest(&$variables) {
  kidneys_node_digest_variables($variables);
}

/**
 * Implements hook_preprocess_HOOK() for node--news--digest.twig.html
 */
function kidneys_preprocess_node__news__digest(&$variables) {
  kidneys_node_digest_variables($variables);
}

/**
 * Implements hook_preprocess_HOOK() for node--forum--digest.twig.html
 */
function kidneys_preprocess_node__forum__digest(&$variables) {
  kidneys_node_digest_variables($variables);

  // Set absolute the forum link.
  if (isset($variables['content']['taxonomy_forums'][0])) {
    $variables['content']['taxonomy_forums'][0]['#url']->setAbsolute(TRUE);
  }
}

/**
 * Implements hook_preprocess_HOOK for field--node--uid.html.twig.
 */
function kidneys_preprocess_field__node__uid(&$variables) {
  if (isset($variables['element']['#view_mode']) && $variables['element']['#view_mode'] == "digest") {
    /** @var Drupal\user\Entity\User $user */
    $user = $variables['user'];
    // In the digest the link to the user must be absolute.
    $options = ['absolute' => TRUE];
    $url = Url::fromRoute('entity.user.canonical', ['user' => $user->id()], $options);
    $link = Link::fromTextAndUrl($user->getAccountName(), $url);
    $variables['items'][0]['content'] = $link->toRenderable();
  }
}

/**
 * Implements hook_preprocess_field().
 */
function kidneys_preprocess_field__field_tags(&$variables) {
  foreach ($variables['items'] as &$item) {
    $item['content']['#options']['attributes']['class'][] = 'badge';
  }
}

/**
 * Implements hook_preprocess_form_element().
 *
 * @See template_preprocess_form_element().
 */
function kidneys_preprocess_form_element(&$variables) {
  $element = &$variables['element'];
  if (!empty($element['#id'])) {
    $variables['id'] = $element['#id'];
  }

  // These fields look different in the login page and in the edit user profile.
  // applying different classes depending in the route.
  $form = [
    'edit-name',
    'edit-mail',
    'edit-pass',
    'edit-current-pass'
  ];
  $variables['route'] =  \Drupal::routeMatch()->getRouteName();
  if (isset($variables['id']) && in_array($variables['id'], $form) && $variables['route'] == 'user.login') {
    $variables['extra_classes'] = ['col-md-6'];
  } else if(isset($variables['id']) && in_array($variables['id'], $form) && $variables['route'] == 'entity.user.edit_form') {
    $variables['extra_classes'] = ['col-md-12'];
  } else {
    $variables['extra_classes'] = [];
  }

  if ($variables['type'] == 'textarea' && isset($variables['element']['#original_description'])) {
    $variables['description']['content'] = $variables['element']['#original_description'];
    $variables['description']['attributes'] = new Attribute();
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for blocks.
 */
function kidneys_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  // Suggest templates based on region.
  if (!empty($variables['elements']['#id'])) {
    $block = Block::load($variables['elements']['#id']);
    array_splice($suggestions, 1, 0, 'block__' . $block->getRegion() . '__' . $variables['elements']['#id']);
    array_splice($suggestions, 1, 0, 'block__' . $block->getRegion());
  }
  // Suggest templates based on content (custom) block (bundle) type.
  if ($variables['elements']['#base_plugin_id'] == 'block_content') {
    $block = $variables['elements']['content']['#block_content'];
    $suggestions[] = 'block__' . $block->bundle();
  }
  // Suggest templates based on view machine name if a Views-provided-block.
  if ($variables['elements']['#base_plugin_id'] == 'views_block') {
    $suggestions[] = 'block__views_block__' . $variables['elements']['content']['#view_id'];
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for fields.
 */
function kidneys_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  // Add the formatter as a field template suggestion.
  array_splice($suggestions, 1, 0, 'field__' . $variables['element']['#field_type'] . '__' . $variables['element']['#formatter']);
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for template_preprocess_user().
 */
function kidneys_theme_suggestions_user_alter(array &$suggestions, array $variables) {
  $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['elements']['#view_mode'];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter for forms.
 */
function kidneys_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['element']['#form_id'];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter for form containers.
 */
function kidneys_theme_suggestions_container_alter(array &$suggestions, array $variables) {
  if (isset($variables['theme_hook_original']) && isset($variables['element']['#id'])) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . str_replace('-', '_', $variables['element']['#id']);
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter for form elements.
 */
function kidneys_theme_suggestions_form_element_alter(array &$suggestions, array $variables) {
  if (!empty($variables['element']['#id'])) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . str_replace('-', '_', $variables['element']['#id']);
  }
  if (!empty($variables['element']['#type'])) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['element']['#type'];
  }
  if (!empty($variables['element']['#id']) && !empty($variables['element']['#type'])) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['element']['#type'] . '__' . str_replace('-', '_', $variables['element']['#id']);
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter for form input elements.
 */
function kidneys_theme_suggestions_input_alter(array &$suggestions, array $variables) {
  if (isset($variables['theme_hook_original']) && isset($variables['element']['#id'])) {
    $suggestions[] = $variables['theme_hook_original'] . '__' . str_replace('-', '_', $variables['element']['#id']);
  }
}


/**
 * Implements template_preprocess_HOOK() for full text fields.
 *
 * - Remove any possible empty paragraph in the end of the text.
 */
function kidneys_preprocess_field__text_long(&$variables) {
  // Removing any possible empty paragraph in the values.
  foreach ($variables['items'] as $key => &$item) {
    $item['content']['#text'] = preg_replace("/\<p\>\&nbsp\;\<\/p\>\s+?$/m","",$item['content']['#text']);
  }
}


/**
 * Implements hook_preprocess_form_element_label().
 */
function kidneys_preprocess_form_element_label(&$variables) {

  // Changing the label of the password fields in the user profile page.
  if (isset($variables['element']['#id']) && $variables['element']['#id'] == 'edit-pass-pass1') {
    $variables['title'] = t('New password');
  }

  if (isset($variables['element']['#id']) && $variables['element']['#id'] == 'edit-pass-pass2') {
    $variables['title'] = t('Confirm new password');
  }
}

/**
 * Implements hook_form_alter().
 */
function kidneys_form_alter(
  &$form,
  \Drupal\Core\Form\FormStateInterface $form_state,
  $form_id) {

  // In this theme the textarea descriptions will be displayed beside the title,
  // we need to save this value in a new variable because drupal deletes the
  // description and prints it in the text-format-wrapper.
  foreach (Element::children($form) as $key) {
    if (isset($form[$key]['#type']) && $form[$key]['#type'] == 'container' &&
      isset($form[$key]['widget'][0]['#base_type'])  &&
      $form[$key]['widget'][0]['#base_type'] == 'textarea'
    ) {
      $form[$key]['widget'][0]['#original_description'] = $form[$key]['widget']['#description'];
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for views_exposed_form.
 */
function kidneys_form_views_exposed_form_alter(
  &$form,
  \Drupal\Core\Form\FormStateInterface $form_state,
  $form_id) {
  if ($form['#id'] !== 'views-exposed-form-search-resources-content-page-1'
      && $form['#id'] !== 'views-exposed-form-search-members-page-1'
      && $form['#id'] !== 'views-exposed-form-search-members-alt-page-1'
      && $form['#id'] !== 'views-exposed-form-search-resources-content-alt-page-1'
  ) {
    return;
  }
  $form['keyword']['#attributes'] = array(
    'placeholder' => t('filter / search by text'),
  );
  $form['keyword']['#title_display'] = 'invisible';
}

/**
 * Implements hook_form_FORM_ID_alter() for comment_comment_forum_form.
 */
function kidneys_form_comment_comment_forum_form_alter(
  &$form,
  \Drupal\Core\Form\FormStateInterface $form_state,
  $form_id
) {
  $form['#attached']['library'][] = 'kidneys/comments-reply-link';
}

/**
 * Implements hook_form_FORM_ID_alter() for user_form.
 */
function kidneys_form_user_form_alter(
  &$form,
  \Drupal\Core\Form\FormStateInterface $form_state,
  $form_id) {

  // It seems to different forms execute this preprocess (for instance user_register_form),
  // let's make sure to only user_form is going to use this preprocess.
  if ($form_id != 'user_form') {
    return;
  }

  $form['personal_digest']['daysoftheweek']['#access'] = false;
  $form['#after_build'][] = 'edit_user_form';

  // Revision box shouldn't be displayed.
  $form['revision_information']['#attributes']['class'][] = 'hidden';

  // Removing the "Views displays" title in the personal digest box.
  $form['personal_digest']['displays']['#header'][0] = '';
  // Adding the help text in the views.
  foreach (Element::children($form['personal_digest']['displays']) as $display) {
    $form['personal_digest']['displays'][$display]['label']['#markup'] .= guts_build_personal_digest_help_text($display);
  }

  $text = (string)$form['personal_digest']['weeks_interval']['#title'];
  $form['personal_digest']['weeks_interval']['#title'] =  t($text . guts_build_personal_digest_help_text('weeks_interval'));

}

/**
 * @param string $display The view display name.
 * @return string help text
 */
function guts_build_personal_digest_help_text($display) {
  $message = '';
  if ($display == 'activity_comment:page_1') {
    $message = 'Content I created or other updates to that content';
  }
  if ($display == 'resources:block_1') {
    $message = 'Updates to Resources section, including Curriculum';
  }
  if ($display == 'activity:block_1') {
    $message = 'Updates to discussion section';
  }
  if ($display == 'personal_digest_news:block_1') {
    $message = 'Updates to News section';
  }
  if ($display == 'weeks_interval') {
    $message = 'Select how often you receive your digests. Applies to all enabled digests listed above';
  }

  if (empty($message)) {
    return '';
  }

  return
    '<div class="form--element--help">' .
    '<i class="glyphicon glyphicon-info-sign"></i>' .
    '<div id="edit-user-picture-0-alt--description" class="help-block">' .
    $message .
    '</div>' .
    '</div>';
}

/**
 * Change the Field order in the user edit form, the first field must be the
 * email field.
 */
function edit_user_form($form, &$form_state) {
  // field_sign_up_announce should be displayed below the personal_digest box.

  $form['account']['mail']['#weight'] = (float)-6;

  // Moving the "other" fields inside the Professional Interests fieldset.
  $form['field_grade_levels_taught']['widget']['field_grade_levels_taught_other'] = $form['field_grade_levels_taught_other'];
  unset($form['field_grade_levels_taught_other']);

  $form['field_i_ve_implemented_these_mod']['widget']['field_ive_implemented_thes_other'] = $form['field_ive_implemented_thes_other'];
  unset($form['field_ive_implemented_thes_other']);

  $form['field_i_ve_participated_in']['widget']['field_ive_participated_in_other'] = $form['field_ive_participated_in_other'];
  unset($form['field_ive_participated_in_other']);

  $form['field_school_setting']['widget']['field_school_setting_other'] = $form['field_school_setting_other'];
  unset($form['field_school_setting_other']);

  $form['field_usage_settings']['widget']['field_usage_settings_other'] = $form['field_usage_settings_other'];
  unset($form['field_usage_settings_other']);

  $form['field_subjects_taught']['widget']['field_subjects_taught_other'] = $form['field_subjects_taught_other'];
  unset($form['field_subjects_taught_other']);

  return $form;
}

/**
 * Implements theme_filefield_sources_list().
 */
function kidneys_filefield_sources_list($variables) {
  $element = $variables['element'];
  $sources = $variables['sources'];

  if ($sources['remote']['label']) {
    $sources['remote']['label'] = t('Fetch by URL');
  }

  $links = array();

  foreach ($sources as $name => $source) {
    $links[] = '<a href="#" onclick="return false;" title="' . $source['description'] . '" id="' . $element['#id'] . '-' . $name . '-source" class="filefield-source filefield-source-' . $name . '">' . $source['label'] . '</a>';
  }
  return '<div class="filefield-sources-list">' . implode(' | ', $links) . '</div>';
}

/**
 * Implements theme_preprocess_details().
 */
function kidneys_preprocess_details(&$variables) {
  $variables['summary_attributes']['class'] = 'hidden';

  $no_panel = [
    'edit-field-address-0',
    'edit-comment-notify-settings',
  ];

  // no need to add the panel and panel-default classes in the Comments notify box.
  if (isset($variables['attributes']['id']) && in_array($variables['attributes']['id'], $no_panel)) {
    return;
  }

  $variables['attributes']['class'][] = 'panel';
  $variables['attributes']['class'][] = 'panel-default';
  $variables['summary_fieldset_wrapper'] = new Attribute();
  $variables['summary_fieldset_wrapper']['class'] = [
    'fieldset-wrapper',
    'panel-body',
  ];
}
/**
 * Add the panel and panel-default classes in the fieldsets excluding the
 * fieldsets related with the professional interested fields.
 */
function kidneys_preprocess_fieldset(&$variables) {
  $variables['fieldset_wrapper']['attributes'] = new Attribute(['class' => ['fieldset-wrapper', 'panel-body']]);

  // Hide the Mailchimp Interested groups field set because it is empty.
  if (isset($variables['attributes']['id']) && $variables['attributes']['id'] == 'edit-field-sign-up-announce-0-value-interest-groups') {
    $variables['attributes']['class'][] = 'hidden';
  }

  // The professional interested fields should have a different classes.
  $two_columns_nested_fields = [
    'field_school_setting',
    'field_subjects_taught',
    'field_usage_settings',
    'field_grade_levels_taught',
    'field_years_of_teaching_experien',
    'field_i_ve_implemented_these_mod',
    'field_i_ve_participated_in',
    'field_need_mentoring_in',
    'field_can_mentor_in',
    'field_languages',
    'field_languages_i_speak',
  ];

  $one_column_nested_fields = [
    'field_i_have',
    'field_student_characteristics',
  ];

  if (isset($variables['element']['#field_name'])) {
    if (in_array($variables['element']['#field_name'], $two_columns_nested_fields)) {
      $variables['attributes']['class'][] = 'col-md-6';
      return;
    }
    elseif (in_array($variables['element']['#field_name'], $one_column_nested_fields)) {
      $variables['attributes']['class'][] = 'col-md-12';
      return;
    }
  }

  if (!isset($variables['element']['#field_name']) || is_null($variables['element']['#field_name'])) {
    $element_keys = array_keys($variables['element']);
    if (array_intersect($two_columns_nested_fields, $element_keys)) {
      $variables['fieldset_wrapper']['attributes']['class'][] = 'row';
    }
  }

  $variables['attributes']['class'][] = 'panel-default';
  $variables['attributes']['class'][] = 'panel';
}

/**
 * Implements hook_preprocess_user__full().
 */
function kidneys_preprocess_user__full(&$variables) {
  $profile_link = Url::fromUri('internal:/user/' . $variables['user']->id() . '/edit');
  $contact_link = Url::fromUri('internal:/user/' . $variables['user']->id() . '/contact');
  $contact_link = ($contact_link->access(\Drupal::currentUser())) ? $contact_link : FALSE;
  $backpack_link = Url::fromUri('internal:/user/' . $variables['user']->id() . '/bookmarks');
  $backpack_link = ($backpack_link->access(\Drupal::currentUser())) ? $backpack_link : FALSE;

  $variables['contact_link'] = $contact_link;
  $variables['profile_link'] = $profile_link;
  $variables['backpack_link'] = $backpack_link;

  $variables['current_user_id'] = \Drupal::currentUser()->id();
  $variables['current_user_roles'] = \Drupal::currentUser()->getRoles();

  $variables['forum_topics'] = views_embed_view('member_posts', 'block');
  $variables['resources'] = views_embed_view('member_posts', 'block_resources');
  $variables['journal'] = views_embed_view('member_posts', 'journal_block');
}

/**
 * Implements hook_preprocess_html().
 *
 * Add a class to the page body tag with the first part of the URL to allow
 * styling by section.
 */
function kidneys_preprocess_html(&$variables) {
  // Retrieve an array which contains the path pieces.
  $current_uri = \Drupal::request()->getRequestUri();
  // Replace all potential separators with a single one to use in explode.
  $parsed_current_uri = str_replace(['/', '?', '=', '&'], '|', $current_uri);
  $fragments = explode('|', $parsed_current_uri);
  if (isset($fragments[1]) && !empty($fragments[1])) {
    $variables['attributes']['class'][] = Html::cleanCssIdentifier(strtolower($fragments[1]));
  }
}

/**
 * Implements hook_preprocess_node() for sidebar resources.
 */
function kidneys_preprocess_node__resource__sidebar(&$variables) {
  $date_formatter = \Drupal::service('date.formatter');
  $created_node = $variables['node']->getCreatedTime();
  $variables['date'] = $date_formatter->formatTimeDiffSince($created_node, ['granularity' => 3]);
}

/**
 * Implements hook_preprocess_field_multiple_value_form().
 * @param $variables
 */
function kidneys_preprocess_field_multiple_value_form(&$variables) {
  if (isset($variables['element']['#field_name']) && $variables['element']['#field_name'] == 'field_links') {
    $variables['table']['#header'][0]['data']['#prefix'] = '<label>';
    $variables['table']['#header'][0]['data']['#suffix'] = '</label>';
  }
}

/**
 * Implements hook_preprocess_file_entity_download_link().
 */
function kidneys_preprocess_file_entity_download_link(&$variables) {
  $file_field = $variables['file']->_referringItem;
  $file_data = $file_field->getValue();
  $description = (isset($file_data['description']) && !empty($file_data['description'])) ? $file_data['description'] : $variables['file']->label();

  $variables['download_link']->setText($description);
  $renderable_link = $variables['download_link']->toRenderable();
  $renderable_link['#attributes']['title'] = $variables['file']->label();
  $variables['download_link'] = $renderable_link;
}

/**
 * Implements hook_preprocess_item_list__roles
 */
function kidneys_preprocess_item_list__roles(&$variables) {
  $variables['context'] = ['list_style' => 'comma-list'];
}

/**
 * Implements hook_preprocess_comment().
 */
function kidneys_preprocess_comment(&$variables) {
  // By default the user picture in the comments is actually the user "compact"
  // view mode, so let's overwrite that and just set the picture.
  /** @var \Drupal\comment\CommentInterface $comment */
  $comment = $variables['elements']['#comment'];
  $account = $comment->getOwner();
  $field_user_picture = $account->get('user_picture')->getValue();
  $default_image = $account->get('user_picture')->getSetting('default_image');

  // Checking if the user has avatar.
  $avatar_uri = '';
  if (isset($field_user_picture[0]['target_id'])) {
    $avatar_uri = File::load($field_user_picture[0]['target_id'])->getFileUri();
  } else if (isset($default_image['uuid'])){
    // Checking if the field has a default image.
    $default_image = \Drupal::entityManager()->loadEntityByUuid('file', $default_image['uuid']);
    $avatar_uri = (!is_null($default_image)) ? $default_image->getFileUri() : '';
  }

  if (!empty($avatar_uri)) {
    $variables['user_picture'] = [
      '#theme' => 'image_style',
      '#style_name' => 'avatar_100x100_',
      '#uri' => $avatar_uri,
    ];
  } else {
    $variables['user_picture'] = [];
  }
}

/**
 * Implements hook_preprocess_node__faq__full().
 */
function kidneys_preprocess_node__faq(&$variables) {
  if (isset($variables['content']['field_faq_category'][0]['#options']['entity'])) {
    /** @var \Drupal\taxonomy\Entity\Term $term */
    $term = $variables['content']['field_faq_category'][0]['#options']['entity'];
    // Overwrite the link with the faq page instead of the taxonomy page.
    $url  = Url::fromUri('internal:/faq/' . $term->id());
    $variables['content']['field_faq_category'][0]['#url'] = $url;
  }
}

/**
 * Implements hook_preprocess_views_view_unformatted().
 */
function kidneys_preprocess_views_view_unformatted(&$variables) {
  // The following lists are listing and must have a line that separte every
  // row.
  $listing_views = [
    'search_resources_content',
    'search-news-content',
    'events'
  ];
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];

  if (in_array($view->id(), $listing_views)) {
    foreach ($variables['rows'] as $row_id => $row) {
      $variables['rows'][$row_id]['attributes']->addClass('row-with-separator');
    }
  }

}
