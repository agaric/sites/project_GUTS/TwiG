<?php

/**
 * @file
 * Profile for Teachers With GUTS Online Professional Development Network.
 */
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Implements hook_help().
 */
function guts_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.guts':
      $output = '';
      $output .= '<h2>' . t('Teachers with GUTS web site: Editor\'s guide') . '</h2>';

      $output .= '<h3>' . t('Editing the user menu') . '</h3>';
      $output .= '<p>' . t('The user menu is implemented as two separate menus which are edited (links added, removed, renamed, or reordered) separately: for <a href=":account_menu_url">logged in account-holders</a> and <a href=":visitor_menu_url">logged out visitors</a>.',
        [
          ':visitor_menu_url' => \Drupal::url('entity.menu.edit_form', ['menu' => 'visitor-menu']),
          ':account_menu_url' => \Drupal::url('entity.menu.edit_form', ['menu' => 'account']),
        ]) . '</p>';

      $output .= '<h3>' . t('Editing the main navigation and other menus') . '</h3>';
      $output .= '<p>' . t('Add, remove, rename, or reorder links <a href=":main_menu_url">here for the main menu</a> or <a href=":overall_menu_url">start here to find any menu to edit</a>.',
        [
          ':main_menu_url' => \Drupal::url('entity.menu.edit_form', ['menu' => 'main']),
          ':overall_menu_url' => \Drupal::url('entity.menu.collection'),
        ]) . '</p>';
      $output .= '<p>' . t('You can also reach edit menu pages through contextual links by hovering over the menu.') . '</p>';
      $output .= '<h3>' . t('Creating or editing a Curriclum Type, Content Type, Curricular Area, and other category') . '</h3>';
      $output .= '<p>' . t('Reach categories in which you can add, remove, rename, or reorder terms at <a href=":taxonomy_admin">Administer » Structure » Taxonomy</a>.',
        [
          ':taxonomy_admin' => \Drupal::url('entity.taxonomy_vocabulary.collection'),
        ]) . '</p>';
      $output .= '<p>' . t('You can also reach edit menu pages through contextual links by hovering over the menu.') . '</p>';
    return $output;
  }
}

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Component\Utility\Html;

/**
 * Implements hook_mail().
 */
function guts_mail($key, &$message, $params) {
  $token_replacements = [
    '@type' => $params['type'],
    '@node_url' => $params['node_url'],
    '@node_title' => $params['node_title'],
    '@submitter_name' => $params['submitter_name'],
    '@submitter_email' => $params['submitter_email'],
  ];
  $options = array(
    'langcode' => $message['langcode'],
  );
  $body_fragment = 'To see the new @type @submitter_name <@submitter_email> has posted:';
  if ($params['has_resource']) {
    $body_fragment = 'A new resource has been posted and a @type discussion has been created:';
    $token_replacements['@type'] = 'resource';
  }
  switch ($key) {
    case 'node_insert':
      $message['subject'] = t('New @type: "@node_title" posted by @submitter_name', $token_replacements, $options);
      $body_template = <<<EOD
Dear Overlord:

Visit:

@node_url

$body_fragment "@node_title".

Sincerely,

Teachers with GUTS
EOD;
      $body = t($body_template, $token_replacements);
      $message['body'] = $body;
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for user_form.
 */
function guts_form_user_form_alter(
  &$form,
  \Drupal\Core\Form\FormStateInterface $form_state,
  $form_id) {
  $user = $form_state->getFormObject()->getEntity();
  // If the id() is null, the \Drupal::currentUSer()->id() will be 0 (anonymous)
  $user_id = $user->id() ?: \Drupal::currentUser()->id();

  $first_time_login = $user->get('field_first_time_login')->value;

  // If it is the first time the user logged into the site, redirect to home on
  // submit.
  if ($first_time_login) {
    $form['actions']['submit']['#submit'][] = 'redirect_home';
  }

  if (isset($form['personal_digest']['#type'])) {
    $form['personal_digest']['#type'] = 'fieldset';
  }

  $form['personal_digest']['#title'] = 'Notification Settings';
  // Removing some unused options from the weeks_interval dropdown.
  unset($form['personal_digest']['weeks_interval']['#options'][2]);
  unset($form['personal_digest']['weeks_interval']['#options'][8]);
  unset($form['personal_digest']['weeks_interval']['#options'][13]);

  // Comment Notify Tweaks.
  if ($user->id() <> 0) {
    $form_state->loadInclude('comment_notify', 'inc');
    /** @var \Drupal\comment_notify\UserNotificationSettings $user_settings */
    $user_settings = \Drupal::service('comment_notify.user_settings');
    $notification_settings = $user_settings->getSetting($user->id(), 'comment_notify');
    if (is_array($notification_settings)) {
      $node_notify_default = $notification_settings['node_notify'] && $notification_settings['comment_notify'];
    }
    else {
      $node_notify_default = $notification_settings;
    }
    $form['comment_notify_settings']['#access'] = FALSE;
    $form['personal_digest']['node_notify'] = [
      '#type' => 'checkbox',
      '#title' => t('Receive comment follow-up notification emails'),
      '#description' => t("Check this to receive email notifications for new comments on your content and content you've commented on."),
      '#default_value' => $node_notify_default,
      '#weight' => 10,
    ];
    $form['actions']['submit']['#submit'][] = '_save_comment_notify';
  }
}

/**
 * Implements hook_form_alter().
 *
 */
function guts_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if ($form_id == 'node_event_form' || $form_id == 'node_event_edit_form') {
    _guts_conditional_show_location($form);
  }
  if ($form_id == 'node_challenge_form' || $form_id == 'node_challenge_edit_form') {
    _guts_conditional_show_practice_set_and_level($form);
  }
}

/**
 * On Event add and edit, show location only if it is 'in person' or 'both'.
 */
function _guts_conditional_show_location(&$form) {
  $form['field_location']['#states'] = [
    'visible' => [
      ':input[name="field_location_type"]' => [
        ['value' => 'inperson'],
        ['value' => 'both'],
      ],
    ],
  ];
}

/**
 * On Practice (Challenge) add and edit, show set and level only if type filled.
 */
function _guts_conditional_show_practice_set_and_level(&$form) {
  $form['field_practice_set']['#states'] = [
    'visible' => [
      ':input[name="field_practice_type"]' => 'filled',
    ],
  ];
  $form['field_practice_level']['#states'] = [
    'visible' => [
      ':input[name="field_practice_type"]' => 'filled',
    ],
  ];
}

/**
 * Save the comment_notify values.
 * @param $form
 * @param $form_state
 */
function _save_comment_notify(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $user = $form_state->getFormObject()->getEntity();
  $node_notify = $form_state->getValue('node_notify');
  \Drupal::moduleHandler()->loadInclude('comment_notify', 'inc', 'comment_notify');
  // Use our own field to save the values in the comment_notify settings.
  if ($node_notify) {
    $notification_node_reply = 1;
    $notification_comment_reply = 1;
  }
  else {
    $notification_node_reply = 0;
    $notification_comment_reply = 0;
  }

  if ($user->id() != 0) {
    /** @var \Drupal\comment_notify\UserNotificationSettings $user_settings */
    $user_settings = \Drupal::service('comment_notify.user_settings');
    $user_settings->saveSettings($user->id(), $notification_node_reply, $notification_comment_reply);
  }
}

/**
 * Redirect user to the home page and set the first time login flag to false.
 */
function redirect_home(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
  $user->set('field_first_time_login', 0, FALSE);
  $user->save();
  $form_state->setRedirectUrl(Url::fromUri('internal:/'));
}

/**
 * Implements hook_ENTITY_TYPE_insert() for nodes.
 *
 * Send e-mail to all in administrator role when forum posts are added.
 * This will include a notice for resources, as these automatically have forum posts.
 *
 * @param Drupal\Core\Entity\EntityInterface $entity
 */
function guts_node_insert(Drupal\Core\Entity\EntityInterface $entity) {
  if ($entity->bundle() !== 'forum') {
    return;
  }
  $params['type'] = $entity->bundle();
  $to = '';
  $overlords = \Drupal::service('entity_type.manager')->getStorage('user')->loadByProperties(['roles' => 'administrator']);
  $separator = '';
  foreach ($overlords as $recipient) {
    $to .= $separator . $recipient->getUsername() . ' <' . $recipient->getEmail() . '>';
    $separator = ',';
  }
  $params['submitter_email'] = $entity->getOwner()->getEmail();
  $params['submitter_name'] = $entity->getOwner()->getUsername();
  // We give a placeholder for message body here and fill it out properly in
  // hook_mail(), where the subject is set.
  $params['body'] = $params['node_url'] = $entity->toUrl()->setAbsolute()->toString();
  $params['node_title'] = $entity->label();
  $params['has_resource'] = ($entity->get('field_resource')->getValue());
  $langcode = \Drupal::currentUser()->getPreferredLangcode();

  $result = \Drupal::service('plugin.manager.mail')->mail('guts', 'node_insert', $to, $langcode, $params, NULL, TRUE);
  if ($result['result'] !== TRUE) {
    \Drupal::logger('mail-log')->error(t('There was a problem sending a new @type email notification to @email.',
        array('@email' => $to, '@type' => $params['type'])));
    return;
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function guts_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'entity_insert') {
    // Move our entity_insert to the end of the list so pathauto has already run.
    // \Drupal::moduleHandler()->getImplementations()
    // iterates through $implementations with a foreach loop which PHP iterates
    // in the order that the items were added, so to move an item to the end of
    // the array, we remove it and then add it.
    $group = $implementations['guts'];
    unset($implementations['guts']);
    $implementations['guts'] = $group;
  }
}


/**
 * Implements hook_entity_insert().
 *
 * Set aliases for default content on insert.
 *
 * Workaround until Default Content's path import is fixed properly, see
 * https://www.drupal.org/node/2710421
 */
function guts_entity_insert(Drupal\Core\Entity\EntityInterface $entity){
  // For our blocks to show on the right page, we need to delete the automatic
  //\Drupal::entityTypeManager()->getStorage('path_alias')->delete(['alias' => '/help/resources']);
  $storage = \Drupal::service('entity_type.manager')->getStorage('path_alias');
  $alias = $storage->loadByProperties(['alias' => '/help/resources']);
  if ($alias) {
    $storage->delete($alias);
  }

  $url_aliases = array(
    'e205b803-eb75-4324-b770-344e84c0c3aa' => '/resources',
  );

  $entity_uuid = $entity->uuid();

  if (isset($url_aliases[$entity_uuid])) {
    $storage = \Drupal::entityTypeManager()->getStorage('path_alias');
    $alias = $storage->create([
      'path' => '/node/' . $entity->id(),
      'alias' => $url_aliases[$entity_uuid],
    ]);
    $alias->save();
  }
}


/**
 * @param $variables
 */
function guts_preprocess_node__forum__digest(&$variables) {
  if ($variables['node']->get('field_resource')->getValue() && is_numeric($variables['node']->get('field_resource')->getValue()[0]['target_id'])) {
    $resource = Node::load($variables['node']->get('field_resource')->getValue()[0]['target_id']);
    if ($resource) {
      $variables['resource_summary'] = strip_tags($resource->get('body')->value);
    }
  }
}

/**
 * Implements hook_preprocess_status_messages().
 */
function guts_preprocess_status_messages(&$variables){
  $warning_messages = [];
  if (isset($variables['message_list']['warning'])) {
    foreach ($variables['message_list']['warning'] as $message) {
      // Hide the errors related with the mailchimp newsletter signup.
      if ($message->__toString() != 'There was a problem with your newsletter signup.') {
        $warning_messages[] = $message;
      }
    }

    $variables['message_list']['warning'] = $warning_messages;

    // In the case to we removed all the messages we can safely unset the
    // warning array.
    if (empty($variables['message_list']['warning'])) {
      unset($variables['message_list']['warning']);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for editor_image_dialog.
 */
function guts_form_editor_image_dialog_alter(&$form, &$form_state, $form_id) {
  // Check the size of the inline images, set a limit if the image is too big.
  $form['#validate'][] = 'guts_limit_max_image_size';
}

/**
 * Check the inline image size, limit it to 350 if is too big.
 */
function guts_limit_max_image_size(&$form, &$form_state) {

  if (!empty($form_state->getValue('fid')[0])) {
    $attributes = &$form_state->getValue('attributes');

    /** @var \Drupal\file\FileInterface $file */
    $file = File::load($form_state->getValue('fid')[0]);
    // Get the URI of the image from the file.
    $uri = $file->getFileUri();

    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = \Drupal::service('image.factory')->get($uri);

    if ($image->isValid()) {
      // Get the original width and height of the image.
      $dimensions = array(
        'width' => $image->getWidth(),
        'height' => $image->getHeight()
      );

      $max_width = 350;
      if ($dimensions['width'] > $max_width) {
        // Calculating the new height.
        $attributes['height'] = floor(($max_width * $dimensions['height']) / $dimensions['width']);
        $attributes['width'] = $max_width;

        // Alter the form so the new size be saved and preserved.
        $form['attributes']['width'] = array(
          '#title' => t('Width'),
          '#type' => 'textfield',
          '#default_value' => $attributes['width'],
          '#required' => FALSE,
        );
        $form['attributes']['height'] = array(
          '#title' => t('Height!'),
          '#type' => 'textfield',
          '#default_value' => $attributes['height'],
          '#required' => FALSE,
        );
      }
    }
  }
}

/**
 * Implements hook_mail_alter().
 *
 * Redirect all the emails to an specific email account:
 *
 * The variable  redirect_all_emails_to can be added in the settings.php file
 * and to that account will be send all the emails. (useful for sites in development)
 */
function guts_mail_alter(&$message) {
  if (!$message['to']) {
    return;
  }
  $redirect_mail  =  Settings::get('redirect_all_emails_to');

  // No need to do something if the setting redirect_all_emails_to doesn't exists.
  if (is_null($redirect_mail)) {
    return;
  }

  $bcc_message = '';
  if (!empty($message['headers']['Bcc'])) {
    $bcc_message = t(' -- Bcc: %bcc', ['%bcc' => $message['headers']['Bcc']]);
    unset($message['headers']['Bcc']);
  }

  $cc_message = '';
  if (!empty($message['headers']['Cc'])) {
    $cc_message = t(' -- Cc: %cc', ['%cc' => $message['headers']['Cc']]);
    unset($message['headers']['Cc']);
  }

  // Alter the subject to have a reference to where the email was initially
  // sent.
  $message['subject'] = t(
    '@original_subject -- [Original To: %original_to] @cc_message @bcc_message',
    [
      '@original_subject' => $message['subject'],
      '%original_to' =>  $message['to'],
      '@cc_message' => $cc_message,
      '@bcc_message' => $bcc_message,
    ]
  );
  $message['to'] = $redirect_mail;
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function guts_menu_local_tasks_alter(&$variables) {
  // There is already a contact form button in the page, no need to have it
  // on the local tasks as well.
  if (isset($variables['tabs'][0]['entity.user.contact_form'])) {
    unset($variables['tabs'][0]['entity.user.contact_form']);
  }
}


/**
 * Implements hook_preprocess_node().
 *
 * @TODO review if this is needed still; if it is running for every full node?
 */
function guts_preprocess_node__page__full(&$variables) {
  // Only embed the curriculum view in the "full" viewmode.
  if (!($curriculum_page = \Drupal::state()->get('curriculum_page',0))) {
    return;
  }

  if ($curriculum_page != $variables['node']->id()) {
    return;
  }

  $view = views_embed_view('curriculum', 'block_1');
  $variables['content']['curriculum'] = $view;

}

/**
 * Implements hook_form_HOOK_ID_alter().
 */
function guts_form_comment_challenge_comment_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $form['field_answer']['#attributes']['class'][] = 'answer';
  $form['add_answer'] = [
    '#type' => 'details',
    '#title' => 'Post your answer: Upload a snapshot of your project and a link to the model',
    '#weight' => $form['field_answer']['#weight'],
    '#open' => TRUE,
  ];
  $form['add_answer']['#attributes']['class'][] = 'answer-details';
  $form['add_answer']['field_answer'] = $form['field_answer'];
  unset($form['field_answer']);
}

/**
 * Implements hook_theme().
 */
function guts_theme($existing, $type, $theme, $path) {
  return array(
    'hypothesis_link' => array(
      'variables' => [
        'label' => NULL,
        'uri' => NULL,
        'filename' => NULL,
        'filemime' => NULL,
        'filesize' => NULL,
      ],
    ),
  );
}

/**
 * Implements hook_comment_links_alter().
 */
function guts_comment_links_alter(array &$links, $entity, array &$context) {
  // If there is not a "reply" link, no need to continue.
  if (!isset($links['comment']['#links']['comment-reply'])) {
    return;
  }

  $account = $entity->getOwner();
  $attributes = [];
  $attributes['data-author-name'] = [$account->getAccountName()];
  $attributes['data-permalink'] = [$entity->permalink()->toString()];
  $attributes['class'] = ['reply-comment-link'];

  $url = Url::fromUri('internal:#comment-form', ['attributes' => $attributes]);
  $links['comment']['#links']['comment-reply']['url'] = $url;
}

/**
 * Implements hook_form_FORM_ID_alter() for user_register_form.
 */
function guts_form_user_register_form_alter(
  &$form,
  \Drupal\Core\Form\FormStateInterface $form_state,
  $form_id
) {
  // New users are subscribed by default to the digest.
  $form['personal_digest']['#access'] = FALSE;
  $form['personal_digest']['displays']['personal_digest_news:block_1']['enabled']['#value'] = 1;
  $form['personal_digest']['displays']['resources:block_1']['enabled']['#value'] = 1;
  $form['personal_digest']['displays']['activity:block_1']['enabled']['#value'] = 1;
  $form['personal_digest']['weeks_interval']['#value'] = 1;
  $form['personal_digest']['node_notify']['#value'] = 1;
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function guts_user_insert(\Drupal\user\UserInterface $entity) {
  // The users by default are subscribed to all the digest settings.
  \Drupal::service('user.data')->set(
    'personal_digest',
    $entity->id(),
    'digest',
    [
      'displays' => [
        'personal_digest_news:block_1' => 0,
        'resources:block_1' => 0,
        'activity:block_1' => 0,
      ],
      'daysoftheweek' => 'Monday',
      'weeks_interval' => 1, // Every week.
    ]
  );
}

/**
 * Implements hook_preprocess_bookmark_link().
 */
function guts_preprocess_bookmark_link(&$variables) {
  if (isset($variables['link']['#attributes']['class'])) {
    if (in_array('content--action--bookmark', $variables['link']['#attributes']['class'])) {
      $variables['link']['#attributes']['title'] = 'Save to Backpack';
    }
    elseif (in_array('content--action--bookmark-delete', $variables['link']['#attributes']['class'])) {
        $variables['link']['#attributes']['title'] = 'Remove from Backpack';
    }
  }
}

/**
 * Return a rendereable array with a login to login or register in the site.
 *
 * optionally can append a text infront of the text.
 *
 * @param $text
 *
 * @return array
 */
function guts_login_message($text = '') {
  // Link to invite the visitors to login or register.
  $current_path = \Drupal::service('path.current')->getPath();
  $current_path = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
  $login = Link::createFromRoute('Log in', 'user.login', [], ['destination' => $current_path])->toString();
  $register = Link::createFromRoute('register', 'user.register', [], ['destination' => $current_path])->toString();
  return [
    '#type' => 'markup',
    '#markup' => t('@login or @register @text', ['@login' => $login, '@register' => $register, '@text' => $text]),
  ];
}

/*
 * Implements hook_preprocess_node__forum__full().
 *
 * Display a link to login/register to leave comments on the discussions.
 */
function guts_preprocess_node__forum__full(&$variables) {
  $variables['login_message'] = guts_login_message("to comment");

  /** @var \Drupal\node\NodeInterface $node */
  $node = $variables['node'];
  $variables['node_reference_summary'] = '';

  /** @var \Drupal\node\NodeInterface $reference */
  $reference = $node->field_resource->entity;
  $variables['reference_summary'] = '';

  // It hasn't a reference so no need to do anything.
  if (is_null($reference)) {
    return;
  }

  // There is a field summary for resources and for news we need to use the
  // summary which is part of the body field.
  if ($reference->getType() == 'resource') {
    $variables['reference_summary'] = $reference->field_summary[0]->view();
  }
  else if ($reference->getType() == 'news') {
    $variables['reference_summary'] = $reference->body[0]->view(array('type' => 'text_summary_or_trimmed'));
  }
}

/**
 * Implements hook_preprocess_node__resource().
 */
function guts_preprocess_node__resource__full(&$variables) {
  $variables['login_message'] = guts_login_message("to view attachments and related links, and/or join the discussion");
}

/**
 * Implements hook_preprocess_node__news().
 */
function guts_preprocess_node__news__full(&$variables) {
  $variables['login_message'] = guts_login_message("to join the discussion");
}

/**
 * Transform a text to a chicago case style.
 *
 * <code>
 *  Before:
 *    This is a text.
 *  After:
 *    This is a Text.
 *
 *  Before:
 *    Hello, I want to thank all the people who take care of abandoned pets.
 *  After:
 *    Hello, I Want to Thank All the People Who Take Care of Abandoned Pets.
 * </code>
 *
 * @param string $text
 *
 * @return strung
 */
function guts_chicago_case_style($text) {
  $text = strtolower($text);

  // No capitalized words.
  $no_capitalized = [
    'is',
    'the',
    'or',
    'of',
    'and',
    'in',
  ];
  $capitalized = array_map('ucwords', $no_capitalized);
  $text = ucwords($text);

  return str_replace($capitalized, $no_capitalized, $text);
}

/**
 * Implements hook_views_query_alter().
 */
function guts_views_query_alter($view, $query) {
  if ($view->id() == 'events') {
    // Traverse through the 'where' part of the query.
    foreach ($query->getWhere() as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {
        if ($condition[0] == 'field_end_date_time') {
          // It seems that search_api's views ignore the Drupal Timezone.
          // what we are doing to do here is the difference between two timezones
          // and subtract that number to the UTC time.
          $timezone = \Drupal::config('system.date')->get('timezone.default');
          $date_utc = New DateTime(DrupalDateTime::createFromTimestamp($condition[1], 'UTC')->format('Y-m-d H:i:s'));
          $date_local = New DateTime(DrupalDateTime::createFromTimestamp($condition[1], $timezone)->format('Y-m-d H:i:s'));
          $diff = $date_utc->getTimestamp() - $date_local->getTimestamp();
          $condition[1] = $condition[1]  - $diff;
        }
      }
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function guts_field_widget_form_alter(&$element, $form_state, $context) {
  // Implements a help text to let the user know how to use the focal_point
  // feature.
  if (get_class($context['widget']) === 'Drupal\image\Plugin\Field\FieldWidget\ImageWidget') {
    $element['#description'] = t('<span>Please select a point to be the center of your image. The image will be automatically cropped to fit the space in which it is shown, centering the point you choose.</span>');
  }
}

/**
 * Implements hook_preprocess_node__HOOK().
 */
function guts_preprocess_node__event(&$variables) {
  // Replace the _ by spaces in the timezones.
  $timezone = $variables['content']['field_time_zone'][0]['#context']['value'];
  $variables['content']['field_time_zone'][0]['#context']['value'] = str_replace('_', ' ', $timezone);
}
