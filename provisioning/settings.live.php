<?php

/**
 * @file
 * Settings for the production environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal-guts',
  'username' => 'guts',
  'password' => 'hdlXeXZyItMh',
  'host' => 'localhost',
  'prefix' => '',
);

$config_directories = array(
    CONFIG_SYNC_DIRECTORY => '../config/default'
);

$settings['trusted_host_patterns'] = array(
  '^teacherswithguts\.org$',
  '^www\.teacherswithguts\.org$',
);

$config['image.settings']['suppress_itok_output'] = TRUE;
$config['image.settings']['allow_insecure_derivatives'] = TRUE;

$settings['hash_salt'] = 'sjmcb9AS_K7QG5fVTgYsUD2VIeeANeKen0XfpRkmcAKYcSlh8WbTWg9Lp2UM5hNGUjAvxGa0Uw';
