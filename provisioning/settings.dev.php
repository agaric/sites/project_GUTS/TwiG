<?php

/**
 * @file
 * Settings for the dev environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'root',
  'password' => '',
  'host' => 'localhost',
  'prefix' => '',
);

$settings['config_sync_directory'] = '../config/default';

$settings['hash_salt'] = 'sjmcb9AS_K7QG5fVTgYsUD2VIeeANeKen0XfpRkmcAKYcSlh8WbTWg9Lp2UM5hNGUjAvxGa0Uw';
$settings['trusted_host_patterns'] = array(
  '^guts\.local$',
);

$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

// Override mail delivery.
$config['mailsystem.settings']['defaults']['sender'] = 'test_mail_collector';
$config['mailsystem.settings']['defaults']['formatter'] = 'test_mail_collector';
