<?php

/**
 * @file
 * Settings for the stage environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'stage-guts',
  'username' => 'stage-guts',
  'password' => 'CTVKuj2PbSzy',
  'host' => 'localhost',
  'prefix' => '',
);

$config_directories = array(
    CONFIG_SYNC_DIRECTORY => '../config/default'
);

$settings['trusted_host_patterns'] = array(
  '^stage\.teacherswithguts\.org$',
);

$config['image.settings']['suppress_itok_output'] = TRUE;
$config['image.settings']['allow_insecure_derivatives'] = TRUE;

$settings['hash_salt'] = 'sjmcb9AS_K7QG5fVTgYsUD2VIeeANeKen0XfpRkmcAKYcSlh8WbTWg9Lp2UM5hNGUjAvxGa0Uw';
#$settings['trusted_host_patterns'] = array();

// Override mail delivery.
$settings['redirect_all_emails_to'] = 'guts@agaric.com';
