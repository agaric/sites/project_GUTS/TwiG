if ENV["ENVIRONMENT"] == "dev" then

  desc "Copy database and files from test and run updates locally."
  task :sync_from_test_and_update => [:sync_from_test, :update_local]

  desc "Copy database and files from test site to local."
  task :sync_from_test do
    sh "./vendor/bin/drush sql-dump > /tmp/paranoia.sql"
    sh "./vendor/bin/drush -y sql-drop"
    sh "export TEMP=/vagrant/.tmp && ./vendor/bin/drush -y sql-sync --structure-tables-key=common @test @self"
    sh "./vendor/bin/drush -y rsync @test:%files/ @self:%files"
  end

  desc "Copy database and files from live and run updates locally."
  task :sync_from_live_and_update => [:sync_from_live, :update_local]

  desc "Copy database and files from live site to local."
  task :sync_from_live do
    sh "./vendor/bin/drush sql-dump > /tmp/paranoia.sql"
    sh "./vendor/bin/drush -y sql-drop"
    sh "export TEMP=/vagrant/.tmp && ./vendor/bin/drush -y sql-sync --structure-tables-key=common @live @self"
    sh "./vendor/bin/drush -y rsync @live:%files/ @self:%files"
  end

  desc "Set admin/admin user/pass."
  task :set_admin_login do
    sh "./vendor/bin/drush user-password admin --password=admin"
  end

  desc "Update local. Run to apply all code updates to your local after a git pull."
  task :update_local => [:css] do
    sh "composer install"
    sh "./vendor/bin/drush cr"
    sh "./vendor/bin/drush -y updb"
    sh "./vendor/bin/drush -y cim"
    sh "./vendor/bin/drupal cr all"
  end

end
